/*************************************************************************** */
/* timer0-pwm3-fade.c                                                        */  
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de PWM en el que se obtiene la resolucion maxima. El tiempo Ton  */
/*  tiene 256 niveles de resolucion. Este ejemplo varia el ciclo de trabajo  */
/*  desde 255 hasta 0 por lo que el brillo de los leds se va desvaneciendo   */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/***************************************************/
/* Generar la senal pwm indicada  por el indice i  */
/* Se realizan varios ciclos para poder verla      */
/* funcionar en los leds                           */
/***************************************************/
void senal(unsigned char Ton)
{
  unsigned int ciclos;

  //-- Solo se envia la senal PWM por los 4 bits menos significativos
  //-- del puerto B, el resto se dejan a 1 para comparar las luminosidades
  for (ciclos=0; ciclos<10; ciclos++) {
    PORTB=0xFF;
    timer0_delay(255-Ton);
    PORTB=0xF0;
    timer0_delay(Ton);
  }
}

void main(void)
{
  unsigned char i;
  TRISB=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Establecer Presscaler 
  PS2=1; PS1=0; PS0=0;

  //-- Generar señales PWM con ciclo de trabajo decreciente. Los leds
  //-- se van atenuando hasta que se apagan por completo
  for (i=255; i>0; i--) {
  	senal(i);
  }
  senal(0);
  
  while(1);
}

