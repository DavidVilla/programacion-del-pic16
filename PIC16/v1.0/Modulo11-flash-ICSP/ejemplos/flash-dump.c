/*************************************************************************** */
/* flash-dump.c                                                              */
/*---------------------------------------------------------------------------*/
/* MEMORIA FLASH                                                             */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Volcar el contenido de la flash por el puerto serie                       */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

#define LED RB1

//-- Numero de palabras a volcar
#define TAM 0xF0

//---------------------------------------------------------
//-- Leer de la memoria flash
//---------------------------------------------------------
//-- ENTRADAS: 
//--   dir: Direccion de la flash donde leer
//-- SALIDAS:
//--   Devuelve la palabra (14 bits) que esta en esa posicion
//-----------------------------------------------------------
unsigned int flash_read(unsigned int dir)
{
  //-- Establecer la direccion
  EEADRH = dir>>8;
  EEADR = dir&0xFF;

  //-- Seleccionar memoria flash
  EEPGD=1;

  //-- Establecer bit de lectura
  RD=1;

  //-- Esperar ciclos basura...
  RD=1;
  RD=1;

  return EEDATH<<8 | EEDATA;
}

void main(void)
{
  unsigned int dato;
  unsigned int i;
    

  //-- Configurar led de la Skypic
  TRISB1 = 0;

  //-- Inicializar puerto serie
  sci_conf();

  //-- Volcado!!!
  for (i=0x00; i<TAM; i++) {
    //-- Sacar 16 palabras por cada linea
    if (i%16==0) sci_write('\n');

    //-- Leer la siguiente direccion de la flash
    dato=flash_read(i);

    //-- Enviarla por el puerto serie
    sci_hexint(dato);
    sci_write(' ');
  }

  //-- Indicar que lectura finalizada
  LED=1;
    
  while(1);
}


