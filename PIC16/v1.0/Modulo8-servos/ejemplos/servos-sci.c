/*************************************************************************** */
/* servo8-sci.c                                                              */
/*---------------------------------------------------------------------------*/
/* CONTROL DE SERVOS                                                         */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para posicionar hasta 8 servos                                    */
/* El usuario puede establecer la posicion de los 8 servos mediante comandos */
/* ASCII por el puerto serie                                                 */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

#define ESTADO_ON  0
#define ESTADO_OFF 1

//-- Prescaler a 64
//-- Ventana temporal para el servo en ticks (2.5ms)
#define T           195

//-- Posiciones de los 8 servos
int pos[]={0,0,0,0,0,0,0,0};

//-- Mascara con la activacion de cada uno de los servos
unsigned char mask[]={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
unsigned char estado=0;
unsigned char servo=0;
char Ton;

void isr() interrupt 0
{
  //-- En este estado se activa el pulso
  if (estado==ESTADO_ON) {

    //-- Calcular el ancho en funcion de los grados
    Ton = (78*pos[servo])/90 + 102;

    //-- Activar la senal del servo actual
    PORTB=mask[servo];

    //-- Dar valor inicial del timer
    TMR0=255-Ton;

    //-- Pasar al siguiente estado cuando llegue la interrupcion
    estado=ESTADO_OFF;
  }
  else {
    //-- Poner a cero todas las senales
    PORTB=0x00;

    //-- Esperar un tiempo Toff
    TMR0=255-T+Ton;

    //-- Pasar al siguiente servo
    servo=(servo + 1)%8;

    //-- Pasar al siguiente estado
    estado=ESTADO_ON;
  }

  //-- Flag de interrupcion a cero
  T0IF=0;
}

/**********************************************/
/* Funcion de pausa                           */
/* ENTRADA: unidades de tiempo para la pausa  */
/**********************************************/
void pausa(unsigned char tiempo)
{
  unsigned char i;
  unsigned int j;
  int temp=0;

  //-- Se realizan dos bucles anidados
  for (i=0; i<tiempo; i++) {
    for (j=0; j<0x8000; j++) {
      //-- Operacion inutil, para consumir tiempo
      temp=temp+1;
    }
  }
}

void menu()
{
  sci_cad("Teclas: 1-8 -> Seleccionar servo activo\n");
  sci_cad("q : servo a posicion 90\n");
  sci_cad("a : Servo a posicion -90\n");
  sci_cad("p : Incrementar en 5 grados\n");
  sci_cad("q : Decrementar en 5 grados\n");
  sci_cad("(spacio): Servo a posicioon 0\n");
  sci_cad("Opcion? ");
}

void main(void)
{
  unsigned char c;
  unsigned char i=0;

  //-- Puerto B de salida
  TRISB=0;

  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler A 64
  PS2=1; PS1=0; PS0=1;

  sci_conf();

  //-- Activar interrupciones
  TMR0IE=1;
  GIE=1;

  //-- Sacar el menu
  menu();

  while(1) {
    c=sci_read();
    switch(c) {
      case ' ':
        pos[i]=0;
        break;
      case 'q':
        pos[i]=90;
        break;
      case 'a':
        pos[i]=-90;
        break;
      case 'p':
        pos[i]+=5;
        if (pos[i]>90) pos[i]=90;
        break;
      case 'o':
        pos[i]-=5;
        if (pos[i]<-90) pos[i]=-90;
        break;
      default:
        if (c>='1' && c<='8') {
          i = c - '1';
          sci_cad("Servo ");
          sci_write(c);
          sci_write('\n');
        }
    } 
  } 

}
