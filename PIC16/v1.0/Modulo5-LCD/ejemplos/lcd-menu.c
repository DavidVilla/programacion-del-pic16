/*************************************************************************** */
/* lcd-menu.c                                                                */ 
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* CONTROL DE UN LCD                                                         */
/*---------------------------------------------------------------------------*/
/* Ejemplo de prueba de los diferentes comandos del LCD. Mediante un menu    */
/* de opciones que sale por el puerto serie, el usuario selecciona los       */
/* comandos a probar                                                         */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

//-- Pines de conexionado para el LCD
#define RW  RA2
#define RS  RA1
#define E   RA0

//-- Definicion de los comandos del LCD
#define INIT     0x38
#define ENCENDER 0x0E
#define HOME     0x02
#define CLS      0x01

//-- Valor inicial del timer0 para hacer una pausa de 10ms
//-- Divisor del prescar a 256
#define T_10ms 61

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/*******************************************************/
/* Pausa                                               */
/* ENTRADA: duracion de la pausa en centesimas (10ms)  */
/*******************************************************/
void delay(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}

/*********************************************/
/* Habilitar el dato escrito en el puerto B  */
/*********************************************/
void enable()
{
  //-- Hay que garantizar un tiempo minimo de 500ns
  //-- A 20Mhz, las instrucciones tardan 200ns.
  //-- Por eso nos curamos en salud y repetimos la instruccion
  //-- tres veces. 
  E=1; E=1; E=1;
  E=0; E=0; E=0;
}

/**********************************/
/* Enviar un comando al LCD       */
/**********************************/
void lcd_cmd(unsigned char cmd)
{
  RS=0;
  PORTB=cmd;
  enable();
  delay(1);
}

/*********************************/
/* Escribir un dato en el lcd    */
/*********************************/
void lcd_write(unsigned char car)
{
  RS=1;
  PORTB=car;
  enable(); 
  delay(1);
}

void menu()
{
  sci_cad("Menu\n");
  sci_cad("----\n");
  sci_cad("1.- CLS\n");
  sci_cad("2.- HOME\n");
  sci_cad("3.- Mover display al imprimir\n");
  sci_cad("4.- Display quieto al imprimir\n");
  sci_cad("5.- Cursor a la izquierda\n");
  sci_cad("6.- Cursor a la derecha\n");
  sci_cad("Opcion: \n");
}

void main(void)
{
  unsigned char c;

  //-- Configurar el lcd
  //-- Puerto B de salida: Datos
  TRISB=0x00;
  //-- Puerto A: Bits RB0, RB1 y RB2 de salida, resto entradas
  TRISA=0x0F8;
  //-- Configurar el puerto A como digital
  ADCON1=0x06;

  //-- Configurar temporizador para hacer pausas
  T0CS=0; PSA=0;
  //-- Configurar el prescaler (division entre 64)
  PS2=1; PS1=1; PS0=1;

  //-- Configurar el sci
  sci_conf();

  //-- Pausa inicial
  delay(1);

  //-- Inicializar lcd (para trabajar a 8 bits)
  RW=0; E=0;
  lcd_cmd(INIT);

  //--  Modo de funcionamiento
  lcd_cmd(0x06);

  //-- Encender el LCD
  lcd_cmd(ENCENDER);

  //-- Cursor a HOME
  lcd_cmd(HOME);

  //-- CLS
  lcd_cmd(CLS);
  
  //-- Sacar el menu
  menu();

  while(1) {
    c=sci_read();
    switch(c) {
      case '1': 
        lcd_cmd(CLS);
        break;
      case '2':
        lcd_cmd(HOME);
        break;
      case '3':
        lcd_cmd(0x07);
        break;
      case '4':
        lcd_cmd(0x06);
        break;
      case '5':
        lcd_cmd(0x10);
        break;
      case '6':
        lcd_cmd(0x14);
        break;
      default:
        lcd_write(c);
    }
  }
}



