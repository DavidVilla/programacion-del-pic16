/*************************************************************************** */
/* timer0-organo.c                                                           */  
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Organo digital que funciona por el puerto serie. El usuario envia las    */
/* teclas del 1 al 8 y podra escuchar las notas musicales en el altavoz      */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

#define DO  106
#define RE  123
#define MI  138
#define FA  144
#define SOL 156
#define LA  167
#define SI  177
#define DO_A 181

unsigned char nota[]={DO,RE,MI,FA,SOL,LA,SI,DO_A};
unsigned char cont=0;
unsigned char tono;

void isr() interrupt 0 
{
  //-- Incrementar contador
  cont++;

  //-- y enviarlo por el puerto B. El bit menos significativo tendra la 
  //-- maxima frecuencia de 10Khz. Los siguientes, la mitad, la cuarta parte
  //-- etc... ya que el contador hace de prescaler
  PORTB=cont;

  //-- Dar valor inicial del timer
  TMR0=tono;

  //-- Quitar flag de interrupcion
  TMR0IF=0;
}

void menu(void)
{
  //-- Nota: Si se prueba desde el Hyperterminal de Windows, posiblemente
  //-- Haya que anadir el caracter '\r' al final de cada linea para
  //-- verlo correctamente
	sci_cad("Organo digital\n");
  sci_cad("Teclas 1-8 se corresponden con la escala musical\n");
  sci_cad("Resto de teclas desactivan el sonido\n");
  sci_cad("Dale maestro!! ");
}

void main(void)
{
  unsigned char c;
  unsigned char i;

  TRISB=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 32
  PS2=1; PS1=0; PS0=0;

  //-- configurar puerto serie
  sci_conf();

  //-- Sacar el menu
  menu();

  //-- Activar interrupciones globales,
  //-- pero todavia no las del timer0
  GIE=1;

  //-- El bucle principal se encarga de leer las teclas recibidas
  //-- y establecer la nota musical
  while(1) {
    //-- Leer la nota
    c=sci_read();

    //-- Solo se aceptan teclas de la '1' a la '8'
    if (c>='1' && c<='8') {
      //-- Calcular el indice a la nota. El DO es la nota 0
      i=c-'1';

      //-- Establecer la nota actual a tocar
      tono=nota[i];

      //-- Activar interrupciones
      TMR0IE=1;
    }
    //-- Cualquier otra tecla desactiva el sonido
    else TMR0IE=0;
  }
}

