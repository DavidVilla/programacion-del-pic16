/*************************************************************************** */
/* ledon2.c                                                                  */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Poner a 1 el bit 1 (RB1) del puerto B. Se enciende el led de la Skypic    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Indicar el pin donde esta el LED en la Skypic
#define LED RB1

//-- Configuracion del LED para su utilizacion
#define CONFIGURAR_LED  TRISB1=0

//-- Activacion/desactivacion del led
#define ON  1
#define OFF 0

void main(void)
{
  CONFIGURAR_LED;

  //-- Activar el bit 1 del puerto B. El led se enciende
  LED=ON;

  //-- Bucle infinito
  while(1);
}

