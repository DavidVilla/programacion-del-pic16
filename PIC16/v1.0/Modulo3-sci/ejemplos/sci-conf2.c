/*************************************************************************** */
/* sci-conf2.c                                                               */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de configuracion del puerto serie a 9600 baudios. Cada vez que   */
/* se recibe un dato proveniente del PC se cambia el estado del led          */
/* La parte de configuracion se ha llevado a la funcion sci_conf()           */
/* Esto permitira reutilizar esta funcion para el resto de ejemplos          */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Led en la Skypic
#define LED RB1

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  volatile unsigned char c;

  //-- Configurar el led de la Skypic
  TRISB1=0;

  //-- Configurar puerto serie
  sci_conf();
  
  while(1) {

    //-- Esperar hasta que se reciba un dato
    while (RCIF==0);

    //-- Leer dato (para que Flag se ponga a 0)
    c=RCREG;

    //-- Cambiar el led de estado
    LED^=1; 
   }

}

