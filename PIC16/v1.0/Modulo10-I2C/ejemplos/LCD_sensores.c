/*******************************************************/
/* Sensor Ultrasonidos                      Junio-2009 */
/* Brujula digital                                     */
/* Muestra la distancia por la pantalla del LCD        */
/* LCD por I2C                                         */
/*-----------------------------------------------------*/

//-- Especificar el pic a emplear
#include <pic16f876a.h>


//-- Definiciones relacionadas con la SkyPic
#define LED			0x02     // Pin del led de la Skypic


#define SRF02 		0xE0     // Direcci�n del sensor de ultrasonidos
#define CMPS03  	0xC0     // Direcci�n del chip de la brujula
#define LCDI2C		0xC6     // Direcci�n del LCD I2C  
#define MOTOR		0xB0     // Direccion de los motores

//-- Valor inicial del timer0 para hacer una pausa de 10ms
//-- Divisor del prescar a 256
#define T_10ms 61

//-- Definicion comandos LCD
#define ENCENDER 20
#define APAGAR   19
#define HOME     1
#define CLS      12
#define CURSOROFF 4

/*
******************************
*     FUNCIONES AUXILIARES   *
******************************
*/


void byte_to_ascii(unsigned char dato, unsigned char *ascii) {
  ascii[0]=(dato/100)+48;
  dato=dato%100;
  ascii[1]=(dato/10)+48;
  ascii[2]=(dato%10)+48;
}

void word_to_ascii(unsigned int dato, unsigned char *ascii) {
  ascii[0]=(dato/100)+48;
  dato=dato%100;
  ascii[1]=(dato/10)+48;
  ascii[2]=(dato%10)+48;
}


void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}


/*******************************************************/
/* Pausa                                               */
/* ENTRADA: duracion de la pausa en centesimas (10ms)  */
/*******************************************************/
void delay(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}



/**********************************************/
/* Libreria Serie                             */
/**********************************************/


// Configurar el puerto serie a N81 y 9600 Baudios  */
void sci_conf(void)
{
  SPBRG = 0x81;  //-- 9600 baudios (con cristal de 20MHz)
  TXSTA = 0x24;  //-- Configurar transmisor
  RCSTA = 0x90;  //-- Configurar receptor
}


// Recibir un caracter por el SCI        
// DEVUELVE: Caracter recibido            
unsigned char sci_read(void)
{
  //-- Eserar hasta que llegue el dato
  while (!RCIF);
  return RCREG;
}


// Transmitir un caracter por el SCI    
// ENTRADAS: Caracter a enviar            

void sci_write(unsigned char car)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (!TXIF);
    
  //-- Hacer la transmision
  TXREG=car;
}



/*
****************************************
*    FUNCIONES DE BAJO NIVEL DEL I2C   * 
****************************************
*/

// Envia Ack
// SDA = 0 
void i2c_SendAck() {
  ACKDT = 0;  // Establece un ACK
  ACKEN = 1;  // Lo envia
}


// Envia Nack para finalizar la recepecion
// SDA = 1
void i2c_SendNack() {
  ACKDT = 1;  // Establece un NACK
  ACKEN = 1;  // Lo envia
}


// verifica ACK
// Mira si en el 9 pulso de reloj (SCL en estado a 1) la se�al SDA est� a 0
unsigned char i2c_CheckACK() {
  if ( ACKSTAT == 0 ) {
    return 0;   // correcto (lo he recibido )
  } else { 
    return 1; // incorrecto (no lo he recibido)
  }
}


// Envia la condicion de STOP
// Libera el BUS I2C, SDA y SCL a nivel alto
// SDA=1 cuando SCL=1. 
void i2c_SendStop() {
  PEN = 1;		// send stop bit
}


// Envia la condicion de START
// Inicializa el Bus I2C, SCL y SDA a nivel bajo
// Estando SCL=1 pone SDA=0, luego pone SCL=0
void i2c_SendStart() {
  SEN = 1;      // send start bit
}

// Espera a que el I2C reciba algun evento
void i2c_WaitMSSP() {
  while (SSPIF == 0);  // Espera evento
  SSPIF=0;             // Limpia FLAG
}

// Repeated start from master
// Queremos transmitir m�s datos pero sin dejar el BUS, es decir
// sin mandar previamente un STOP. O por ejemplo si queremos mandar un STOP y seguidamente un
// START para que ning�n otro dispositivo ocupe la l�nea usaremos esto.
void i2c_SendRStart() {
  RSEN=1;
}


// Leer Byte por el I2C
// Devuelve byte leido
unsigned char i2c_ReadByte() {
  RCEN=1;        // Activar el RCEN

  i2c_WaitMSSP();
  return SSPBUF;
}




// Envia un Dato por el Bus I2C
// Entrada el dato a enviar
void i2c_SendByte(unsigned char dato) {
	SSPBUF=dato;
}



// Fallo en el I2C
void i2c_fail() {
  i2c_SendStop();
  i2c_WaitMSSP();
  sci_write('f');   // @@@ Para depurar
}



/*
**************************************
*   FUNCIONES DE ALTO NIVEL del I2C  *
**************************************
*/

/* Configurar I2C
  Configuramos el I2C como Master
  y a una velocidad de 1Mhz
*/

void i2c_configure() {
  // configuramos SCL y SDA como pines de entrada
  TRISC=TRISC | 0x18;  // 1 entrada / 0 salida  
  
  SSPSTAT=0x0;  
  SSPSTAT=SSPSTAT | 0x80;  // SMP=1 Slew rate disable for 100Kh  @@@
                           // CKE=0 (modo maestro I2C) y UA=0 (7 bits)

  SSPCON=0x08;     // I2C master mode (uso la formula del reloj con SSPAD)  
  SSPCON=SSPCON | 0x20;  // enable I2C  SSPEN=1

  SSPCON2=0x00;   // no se usa por ahora
  // velocidad = FOSC / ( 4 * (sspad + 1) )
  // Fosc=20Mhz
  
  SSPADD=49;  // 49->100Khz  @@@
  //SSPADD=24; // 24 -> 400khz   @@@
  
  // Limpia Flags de eventos
  SSPIF=0;  //Limpia flag de eventos SSP 
}


// manda un byte al dispositivo i2c
unsigned char i2c_write_byte(unsigned char address, unsigned char reg, unsigned char dato)
{

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  	

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;  // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg);
  i2c_WaitMSSP(); 
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� mando el dato 
  i2c_SendByte(dato); 
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;    // Ha habido un error, salgo de la rutina
  }

  // 6� termino el envio
  i2c_SendStop();
  i2c_WaitMSSP();

  return 1;  // Byte mandado correctamente
}




// Lee 1 byte1  del dispositivo i2c
// El dato leido lo devuelve por el parametro dato
unsigned char i2c_read_byte(unsigned char address, unsigned char reg, unsigned char *dato)
{

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg); 
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� Repeated start
  i2c_SendRStart();
  i2c_WaitMSSP();  

  // 4� mando direccion indicando lectura
  i2c_SendByte(address+1);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 5� leo el byte 
  *dato=i2c_ReadByte(); 

  // 6� Mando NACK
  i2c_SendNack();
  i2c_WaitMSSP();  

  // Mando el Stop
  i2c_SendStop();
  i2c_WaitMSSP();  

  return 1;   // Lectura correcta
}

// Lee 2 bytes consecutivos  del dispositivo i2c
// El dato leido lo devuelve por el parametro dato
unsigned char i2c_read_word(unsigned char address, unsigned char reg, unsigned int *dato)
{
  unsigned char tmp_H, tmp_L;

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  	

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg); 
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� Repeated start
  i2c_SendRStart();
  i2c_WaitMSSP();  

  // 4� mando direccion indicando lectura
  i2c_SendByte(address+1);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 5� leo el 1 byte 
  tmp_H=i2c_ReadByte(); 

  // 6� Mando ACK
  i2c_SendAck();
  i2c_WaitMSSP();  

  // 7� leo el 2 byte 
  tmp_L=i2c_ReadByte(); 
  
  // 6� Mando NACK
  i2c_SendNack();
  i2c_WaitMSSP();  

  // Mando el Stop
  i2c_SendStop();
  i2c_WaitMSSP();  

  *dato=(tmp_H*256) + tmp_L;
  
  return 1;   // Lectura correcta
}


/*
*******************************************
*  FUNCIONES DEL SENSOR DE ULTRASONIDOS   *   
*******************************************
*/


// Ajusta el sensor
void srf02_adjust() {
  i2c_write_byte(SRF02, 0, 0x60);
}



// Leer la version del Firmware
// En este sensor devuelve 0x05
unsigned char srf02_read_version() {
  unsigned char dato;

i2c_read_byte(SRF02,0x00,&dato);

return dato;
}


// En el registro 1 se debe leer 0x18
unsigned char srf02_check() {
  unsigned char dato;

  i2c_read_byte(SRF02,0x01,&dato);
  return dato;
}


// Iniciamos una medida en centimetros
void srf02_start() {
  i2c_write_byte(SRF02, 0, 0x51);
}

// Obtenemos el byte bajo de la lectura
unsigned char srf02_read_low() {
  unsigned char dato;

  i2c_read_byte(SRF02,0x03,&dato);

  return dato;
}

// Obtenemos el byte alto de la lectura
unsigned char srf02_read_high() {
  unsigned char dato;
  
  i2c_read_byte(SRF02,0x02,&dato);
  
  return dato;
}



/****************************** 
      Funciones de alto nivel
	  para el sensor cmps03
	  (brujula)
******************************/

unsigned char cmps03_read_version() {
  unsigned char res; 
  i2c_read_byte(CMPS03,0x00, &res);

  return res;
}

unsigned char cmps03_read_dir_short() {
  unsigned char res; 
  i2c_read_byte(CMPS03,0x01,&res);
  return res;
}


unsigned int cmps03_read_dir_long() {
  unsigned int res;
  
  i2c_read_word(CMPS03,0x02,&res);

  return res;
}


/****************************** 
      Funciones de alto nivel
	  para el driver de motores
	  (MD03)
******************************/

void md03_start() {

  i2c_write_byte(MOTOR, 2, 255);    // set speed to maximun
  i2c_write_byte(MOTOR, 0, 0x1);    // apply speed 
}

void md03_stop() {
  i2c_write_byte(MOTOR, 2, 0);    // set speed to cero
  i2c_write_byte(MOTOR, 0, 0);    // apply speed 
}


/*
********************************************
*      Libreria LCD I2C                    *
********************************************
*/


/**********************************/
/* Enviar un comando al LCD       */
/**********************************/
void lcd_cmd(unsigned char cmd)
{
  i2c_write_byte(LCDI2C, 0, cmd);
  delay(1);
}

/*********************************/
/* Escribir un dato en el lcd    */
/*********************************/
void lcd_write(unsigned char car)
{
  i2c_write_byte(LCDI2C, 0, car);
  delay(1);
}


//----------------------------
//- Comienzo del programa  
//----------------------------

void main(void)
{
  unsigned char dato;
  unsigned char ascii[3]; 
  unsigned int  dir;


  //-- Puerto B de salida: Datos
  TRISB=0x00;
  //-- Puerto A: Bits RB0, RB1 y RB2 de salida, resto entradas
  TRISA=0x0F8;
  //-- Configurar el puerto A como digital
  ADCON1=0x06;

  //-- Configurar temporizador para hacer pausas
  T0CS=0; PSA=0;
  //-- Configurar el prescaler (division entre 64)
  PS2=1; PS1=1; PS0=1;

  //-- Pausa inicial
  delay(100);

  //-- Configurar el puerto serie
  sci_conf();

  //-- Configurar el I2C
  i2c_configure();

  //-- Inicializar lcd (para trabajar a 8 bits)

  //-- Encender el LCD
  lcd_cmd(ENCENDER);

  //--QUita el cursor
  lcd_cmd(CURSOROFF);
  //-- Cursor a HOME
  lcd_cmd(HOME);

  //-- CLS
  lcd_cmd(CLS);

  //-- Hago una pausa antes de empezar
  delay(10);
  
  // auto ajuste del sensor
  srf02_adjust();

  while (1) {

    // Leemos distancia
    srf02_start();
    delay(10);
    dato=srf02_read_low();

    // Convierte a ASCII
    byte_to_ascii(dato, ascii);

    // La sacamos por el display
    lcd_write(ascii[0]);
    lcd_write(ascii[1]);
    lcd_write(ascii[2]);
    lcd_write(' ');
    // la enviamos por el puerto serie
    sci_write(ascii[0]);
    sci_write(ascii[1]);
    sci_write(ascii[2]);
    sci_write(' ');

    // Leemos la direccion
    dir=cmps03_read_dir_long(); 
    // dividimos entre mil para tener lectura de 0 a 360
    dir=dir/10;
    word_to_ascii(dir,ascii); // Ojo, solo convertimos valores por debajo de 1000. (3 ascii)
    
    // La sacamos por el display
    lcd_write(ascii[0]);
    lcd_write(ascii[1]);
    lcd_write(ascii[2]);

    // la enviamos por el puerto serie
    sci_write(ascii[0]);
    sci_write(ascii[1]);
    sci_write(ascii[2]);
    sci_write(' ');

    // Retorno de carro
    lcd_cmd(HOME);

    // Seg�n el estado de la br�jula enciendo o apago el motor
    if (dir>=180) {
      md03_start();
    } else {
      md03_stop();
    }

    //-- Habria que meter un WDT
    delay(50);  // mientras tantos usamos una pausa
    }
} 
