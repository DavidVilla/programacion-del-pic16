/*************************************************************************** */
/* sci-menu.c                                                                */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de programa interactivo. Muestra un menu de opciones para que    */
/*  el usuario las pueda seleccionar desde un terminal del PC                */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

//-- Especificar el pic a emplear
#include <pic16f876a.h>

#define LED RB1

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/***********************************************************************/
/* Leer un byte del SCI. Esta funcion se queda esperando hasta que     */
/* algun caracter llegue                                               */
/***********************************************************************/
unsigned char sci_read()
{
  //-- Esperar hasta que se reciba un dato
  while (RCIF==0);
  
  return RCREG;
}

/********************************************/
/* Enviar un byte por el SCI                */
/********************************************/
void sci_write(unsigned char dato)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (TXIF==0);
    
  //-- Hacer la transmision
  TXREG=dato;
}

void sci_cad(unsigned char *cad)
{
  unsigned char i=0;

  while (cad[i]!=0) {
    sci_write(cad[i]);
    i++;
  }
}


/******************************/
/* Sacar el menu de opciones  */
/******************************/
void menu(void)
{
  //-- Nota: Si se prueba desde el Hyperterminal de Windows, posiblemente
  //-- Haya que anadir el caracter '\r' al final de cada linea para
  //-- verlo correctamente
  sci_cad("Menu\n");
  sci_cad("----\n");
  sci_cad("1.- Cambiar el estado del led\n");
  sci_cad("2.- Poner todo el puerto B a 1\n");
  sci_cad("3.- Reset del puerto B\n");
  sci_cad("4.- Sacar este menu otra vez\n");
  sci_cad("Opcion? ");
}

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned char c;

  //-- Puerto B de salida
  TRISB=0;

  //-- Configurar las comunicaciones serie
  sci_conf();
  
  //-- Sacar el menu
  menu();
  
  while(1) {
    //-- Esperar a que llegue opcion del usuario
    c=sci_read();

    //-- Segun la tecla pulsada...
    switch(c) {
      case '1':
        LED^=1; 
        break;
      case '2':
        PORTB=0xFF;
        break;
      case '3':
        PORTB=0x00;
        break;
      case '4':
        menu();
        break;
    }
  }
}

