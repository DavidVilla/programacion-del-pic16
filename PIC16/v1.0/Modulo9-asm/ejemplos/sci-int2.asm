;****************************************************************************
;*  sci-int2.asm                                                            *
;---------------------------------------------------------------------------*
; Ejemplo para la tarjeta SKYPIC                                            *
;---------------------------------------------------------------------------*
; Prueba de la interrupcion de recepcion del SCI                            *
; Se hace eco de todo lo recibido, mediante interupciones. El programa      *
; principal hace parpadear el led                                           *
;---------------------------------------------------------------------------*
;  LICENCIA GPL                                                             *
;****************************************************************************

;-- Establecer el PIC a emplear
  INCLUDE "p16f876a.inc"

;---------------------
; VARIABLES
;---------------------
  cblock  0x20
    CONTH
    CONTL
    save_w
    save_status
  endc

  ORG 0
  CLRF    STATUS
  MOVLW   0
  MOVWF   PCLATH
  GOTO    start


;--- Punto de entrada de interrupciones
  ORG 0x04  
  
  MOVWF save_w        ;-- Guardar valor del registro W
  SWAPF STATUS,W     ;-- Guardar valor registro STATUS
  MOVWF save_status
    
    ;-- Hacer eco de todo lo que llega
  CALL leer_car
  CALL enviar
    
   ;--   Recuperar contexto
  SWAPF save_status,W
  MOVWF STATUS
  SWAPF save_w, F
  SWAPF save_w, W

  RETFIE

;--- Comienzo del programa
start

;-- Configuracion del puerto serie
  BSF STATUS,RP0    ; Acceso al banco 1
  MOVLW 0x81        ; Velocidad: 9600 baudios
  MOVWF SPBRG

  MOVLW 0x24
  MOVWF TXSTA       ; Configurar transmisor

  BCF STATUS,RP0    ; Acceso al banco 0		
  MOVLW 0x90        ; Configurar receptor
  MOVWF RCSTA

;-- Configurar Bit RB1 para salida
  BSF STATUS,RP0    ; Acceso al Banco 1
  BCF TRISB,1       ; RB1 como salida

;-- Configurar las interrupciones
  BSF PIE1,RCIE     ; Activar interrupcion del receptor SCI
  BSF INTCON,PEIE   ; Activar interrupci�n de perif�ricos
  BSF INTCON,GIE    ; Activar interrupciones globales

  BCF STATUS,RP0    ; Acceso al banco 0
  BSF PORTB,1       ; Encender led

;------------------------
;-    BUCLE PRINCIPAL
;------------------------
main
  ;;-- Cambiar el led de estado
  MOVLW 0x02
  XORWF PORTB,F

  ;; -- Hacer una pausa
  CALL pausa

  GOTO main

;**************************************************
;* Recibir un caracter por el SCI
;-------------------------------------------------
; SALIDAS:
;    Registro W contiene el dato recibido
;**************************************************
leer_car
  BTFSS PIR1,RCIF   ; RCIF=1?
  GOTO leer_car     ; no--> Esperar

  ;-- Leer el caracter
  MOVFW RCREG       ; W = dato recibido

  RETURN

;*****************************************************
;* Transmitir un caracter por el SCI               
;*---------------------------------------------------
;* ENTRADAS:
;*    Registro W:  caracter a enviar         
;*****************************************************
;-- Esperar a que Flag de listo para transmitir este a 1
enviar
wait
  BTFSS PIR1,TXIF   ; TXIF=1?
  goto wait         ; No--> wait

  ;; -- Ya se puede hacer la transmision
  MOVWF TXREG
  RETURN

;******************************************************************
; Hacer una pausa. Se utiliza una variable de 16 bits,
; que hace de contador. 
; Esta constituida por CONTH (parte alta) y CONTL (parte baja)
; El contador se inicializa a 0xFFFF y se va decrementando
; Se hace con dos bucles anidados, uno que decrementa CONTL y
; cuando llega a cero decrementa CONTH y repite el bucle
;******************************************************************
pausa
  MOVLW 0xFF    ; Inicializar parte alta contador
  MOVWF CONTH

buclel
  MOVLW 0xFF    ; Inicializar parte baja contador
  MOVWF CONTL
repite
  DECFSZ CONTL,F    ; Decrementa CONTL �CONTL=0?
  goto repite       ;  NO--> Repite

  DECFSZ CONTH,F    ; Decrementa CONTH �CONTH=0?
  goto buclel       ; No--> Ve a buclel

;-- Si se ha llegado aqu� es porque el contador ha llegado a 0000
;-- (CONTH=0 y CONTL=0)

  RETURN

  END
