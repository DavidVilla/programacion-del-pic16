;****************************************************************************
;*  pulsador.asm                                                            *
;---------------------------------------------------------------------------*
; Ejemplo para la tarjeta SKYPIC                                            *
;---------------------------------------------------------------------------*
;  Leer el estado del pulsador. Cuando se aprieta se enciende el led        *
;---------------------------------------------------------------------------*
;  LICENCIA GPL                                                             *
;****************************************************************************

;-- Establecer el PIC a emplear
  INCLUDE "p16f876a.inc"

  ORG 0
  ;-- Codigo de inicializacion para bootloader
  CLRF    STATUS
  MOVLW   0
  MOVWF   PCLATH
  GOTO    start
  
start
;-- Configurar puerto B: RB0 entrada, RB1 para salida
  BSF STATUS,RP0  ; Cambiar al banco 1
  BCF TRISB,1     ; RB1 salida

;-- Cambiar al banco 0
  BCF STATUS,RP0

bucle
  BTFSC PORTB,0   ; Pulsador apretado? RB0=0?
  goto no_pulsado ; No--> apagar led

;-- Pulsador apretado
  BSF PORTB,1     ; Encender led
  goto bucle
 
no_pulsado
  BCF PORTB,1     ; Apagar led
  goto bucle

END
