/*************************************************************************** */
/* sci-contadores.c                                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de interrupcion de transmision. Mediante interrupciones se        */
/* envian los numeros del 0 al 9 por el puerto serie. A la vez el bucle      */
/* pricipal incrementa un contador en los leds                               */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

unsigned char i=0;

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/*****************************************/
/* Rutina de atencion a la interrupcion  */
/*****************************************/
void isr() interrupt 0 
{
  //-- Enviar el siguiente digito ASCII (entre '0' y '9')
  //-- Para ello sumamos al contador i (que toma valores entre 0 y 9)
  //-- el valor ASCII correspondiente al digito '0'
  TXREG=i+'0';

  //-- Incrementar contador. Cuando llega a 10 pasa a 0
  //-- % es el operador modulo. 
  i=(i+1)%10;   
 
}

/**********************************************/
/* Funcion de pausa                           */
/* ENTRADA: unidades de tiempo para la pausa  */
/**********************************************/
void pausa(unsigned char tiempo)
{
  unsigned char i;
  unsigned int j;
  int temp=0;

  //-- Se realizan dos bucles anidados
  for (i=0; i<tiempo; i++) {
    for (j=0; j<0x8000; j++) {
      //-- Operacion inutil, para consumir tiempo
      temp=temp+1;
    }
  }
}

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned char cont=0;

  //-- Configurar puerto B para salida
  TRISB=0;

  //-- Configurar puerto serie
  sci_conf();

  //-- Activar las interrupciones
  TXIE=1;
  PEIE=1;
  GIE=1;
  
  //-- Bucle principal
  while(1) {
    PORTB=cont;  //-- Sacar contador por los leds
    pausa(4);    //-- Esperar
    cont++;      //-- Incrementar contador
  }

}

