/*************************************************************************** */
/* timer0-pwm-int.c                                                          */  
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de generacion de PWM mediante interrupciones. El brillo de los   */
/* leds se va desvaneciendo                                                  */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define ESTADO_ON  0
#define ESTADO_OFF 1

unsigned char estado=0;
unsigned char T0ini;

//-- Interfaz entre la rutina de atención a la int.
unsigned char Ton=0;             //-- Senal PWM
volatile unsigned char cont=0;   //-- Contador de ciclos

/*****************************************/
/* Rutina de atencion a la interrupcion  */
/*****************************************/
void isr() interrupt 0 
{
  //-- En esta estado se activa el pulso
  if (estado==ESTADO_ON) {

    //-- Activar todos los leds
    PORTB=0xFF;

    //-- Valor para que esté activo durante Ton ticks
    T0ini=255-Ton;

    //-- Pasar al estado siguiente
    estado=ESTADO_OFF;
  }
  //---- En este estado se desactivan los pulsos
  else {

    //-- Desactivar todos los leds
    PORTB=0x00;

    //-- Valor para que esté activo durante Toff ticks
    T0ini=Ton;

    //-- Pasar al estado inicial
    estado=ESTADO_ON;

    //-- Se ha generado un ciclo más
    cont++;
  }

  //-- Dar valor inicial del timer
  TMR0=T0ini;

  //-- Quitar flag de interrupcion
  TMR0IF=0;
}

void main(void)
{
  unsigned char i;

  TRISB=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 32
  PS2=1; PS1=0; PS0=0;

  //-- Activar interrupciones
  TMR0IE=1;
  GIE=1;

  //-- El pwm se esta generando mediante interrupciones
  //-- En el bucle principal haremos que los leds se vayan desvaneciendo
  for (i=255; i>0; i--) {

    //-- Genera la señal pwm 
    Ton=i; 
    //-- POner contador de ciclos a cero
    cont=0;

    //-- La senal se genera mediante int. Mientras tanto esperamos hasta
    //-- que hayan pasado 10 ciclos. Aquí el PIC podría estar haciendo 
    //-- cualquier otra tarea
    while(cont<10);
  }

  //-- Deshabilitar interrupciones
  TMR0IE=0;

  //-- Poner a 0 todos los leds
  PORTB=0;

  while(1);
}

