/*************************************************************************** */
/* Juan Gonzalez Gomez. Abril-2009                                           */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* TIMER0. Ejemplo de generacion de una onda cuadrada de 1KHz de frecuencia  */
/* La señal se saca por el bit RB1, donde se encuentra el led de la skypic   */
/* Si se conecta directamente un altavoz al pin RB1 se podra oir un pitido   */
/*---------------------------------------------------------------------------*/
/* Calculos:                                                                 */
/*  -> Generacion de una onda cuadrada de 1Khz. El periodo es T=1/1Khz = 1ms */
/*  Durante 0.5ms la señal estará a 1 y durante 0.5ms a 0, por tanto hay que */
/*  hacer que el temporizador se desborde cada 0.5ms.                        */
/*  -> La formula general para obtener una duracción de Dur micro-segundos   */
/*  es:                                                                      */
/*       TMR0 = 256 - round[ FOSC(Mhz)*Dur(us)/(4*DIV) ];                    */
/*  donde:                                                                   */
/*     -FOSC es la frecuencia del cristal de cuarzo en Mhz. Para la Skypic   */
/*      es de FOSC=20                                                        */
/*     -DIV = Valor del divisor para el prescaler. Puede ser 2,4,8,16,32,    */
/*            64,128,256                                                     */
/*     -Dur = Duracion en micro-segundos                                     */
/*                                                                           */
/*  Para conseguir una duracion de 0.5ms = 500 us con una skypic, la formula */
/* queda:                                                                    */
/*      TMR0 = 256 - round [ 20*500/(4*DIV)] = 256 - round[2500/DIV]         */
/*  Se pueden emplear diferentes valores para el divisor. Son validos todos  */
/*  aquellos en los que la operacion 2500/DIV de un número de 8 bits         */
/*  (y por tanto menor a 256)                                                */
/*  Son por tanto validos los valores: DIV = 16, 32, 64, 128 o 256           */
/*                                                                           */
/*  Tomando DIV=16, obtenemos que:                                           */
/*    TMR0= 256 - round[2500/16] = 256 - round[156.25] = 256 - 156 = 100     */
/*---------------------------------------------------------------------------*/
/* RESULTADO:                                                                */
/*  -> Para obtener una senal de 1Khz asignamos los siguientes valores:      */
/*  -> Divisor: 16 --> PS2=0, PS1=1, PS0=1                                   */
/*  -> TMR0 = 100                                                            */
/*---------------------------------------------------------------------------*/
/* CALCULOS AUTOMATICOS USANDO OCTAVE/MATLAB                                 */
/*                                                                           */
/*  Usar el script timer0_TMR0(). Nos da todas las alternativas posibles     */
/*  asi como el error cometido                                               */
/*                                                                           */
/*  > timer0_TMR0_frec(20, 1000);                                            */
/*                                                                           */
/*  TMR0= 100, PS2:0= 011, Div=  16, Frec= 1001.6 Hz, Error= -1.60 Hz        */
/*  TMR0= 178, PS2:0= 100, Div=  32, Frec= 1001.6 Hz, Error= -1.60 Hz        */
/*  TMR0= 217, PS2:0= 101, Div=  64, Frec= 1001.6 Hz, Error= -1.60 Hz        */
/*  TMR0= 236, PS2:0= 110, Div= 128, Frec= 976.6 Hz, Error= 23.44 Hz         */
/*  TMR0= 246, PS2:0= 111, Div= 256, Frec= 976.6 Hz, Error= 23.44 Hz         */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valor a asignar al registro TMR0 para conseguir una senal de 1Khz
//-- Cuando el prescaler tiene un divisor de 16
#define VALOR_1KHz  100

void main(void)
{
  //-- configurar pin RB1 para salida
  //-- Sera por donde salga la senal de 1Khz
  TRISB1=0;
  
  //-- Configurar el timer0 en modo temporizador
  T0CS=0;
  PSA=0;

  //-- Configurar el prescaler e inicializar el temporizador con el 
  //-- valor correcto
  PS2=0; PS1=1; PS0=1; 
  TMR0=VALOR_1KHz;

  //-- Bucle principal. La onda se genera indefinidamente
  while(1) {

    //-- Poner la senal a '1'
    RB1=1;

    //-- Esperar a que transcurra el primer semiperiodo (0.5ms)
    while(T0IF==0);

    //-- Inicializar el temporizador
    T0IF=0;
    TMR0=100;

    //-- Poner la senal a nivel bajo
    RB1=0;

    //-- Esperar a que transcurra el segundo semiperiodo
    while(T0IF==0);

    //-- volver a inicializar el temporizador
    T0IF=0;
    TMR0=100;
  }


}


