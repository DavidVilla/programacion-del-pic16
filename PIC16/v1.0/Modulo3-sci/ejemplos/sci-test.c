/*************************************************************************** */
/* sci-test.c                                                                */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de uso de sci.h. En vez de definir las funciones del puerto serie */
/* en el propio codigo, se usan las que estan definidas en sci.h             */
/* Este programa de ejemplo simplemente hace eco de todo lo que recibe       */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned char c;

  //-- Configurar el puerto B para salida
  TRISB=0;

  //-- Configurar puerto serie
  sci_conf();
  
  while(1) {

    //-- Leer dato (se queda esperando hasta que llega)
    c=sci_read();

    //-- Sacar dato recibido por los leds
    PORTB=c;

    //-- Hacer eco del carácter recibido. Enviarlo de vuelta al PC
    sci_write(c);
    
  }

}

