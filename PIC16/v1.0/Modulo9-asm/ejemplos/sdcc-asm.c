/*************************************************************************** */
/* sdcc-asm.c                                                                */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de invocacion de instrucciones en ensamblador desde un programa  */
/* en C. Se hace parpadear el led                                            */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valor inicial del TMR0 para realizar una pausa de 10ms
//-- Divisor del prescar a 256
#define T0INI 61

#define LED RB1

/******************************/
/* Hacer una pausa de 10ms    */
/******************************/
void pausa_10ms()
{
  //-- Dar valor inicial del timer
  TMR0=T0INI;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);
}

void pausa(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    pausa_10ms();
}

void main(void)
{
  TRISB1=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 256
  PS2=1; PS1=1; PS0=1;

  RB1=0;

  while(1) {

    //-- Comienzo de la region en ensamblador
    _asm      
      MOVLW 0x02
      XORWF PORTB,F
    _endasm;

    //-- Fin de la region en ensamblador

    pausa(10);
  }
}

