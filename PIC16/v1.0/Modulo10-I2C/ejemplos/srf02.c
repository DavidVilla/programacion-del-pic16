/*******************************************************/
/* Sensor Ultrasonidos                      Junio-2009 */
/*-----------------------------------------------------*/
/* Programacion del BUS I2C para el PIC 16F876A        */
/*******************************************************/

//-- Especificar el pic a emplear
#include <pic16f876a.h>


//-- Definiciones relacionadas con la SkyPic
#define LED   0x02     // Pin del led de la Skypic


//-- declaracion de funciones del SRF02 --//

#define SRF02   0xE0     // Direccion del sensor SRF02

void srf02_adjust();
unsigned char srf02_check();
unsigned char srf02_read_version();
void srf02_start();
unsigned char srf02_read_low();
unsigned char srf02_read_high();


/**********************************************/
/* Libreria Serie                             */
/**********************************************/


// Configurar el puerto serie a N81 y 9600 Baudios  */
void sci_conf(void)
{
  SPBRG = 0x81;  //-- 9600 baudios (con cristal de 20MHz)
  TXSTA = 0x24;  //-- Configurar transmisor
  RCSTA = 0x90;  //-- Configurar receptor
}


// Recibir un caracter por el SCI        
// DEVUELVE: Caracter recibido            
unsigned char sci_read(void)
{
  //-- Eserar hasta que llegue el dato
  while (!RCIF);
  return RCREG;
}


// Transmitir un caracter por el SCI    
// ENTRADAS: Caracter a enviar            

void sci_write(unsigned char car)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (!TXIF);
    
  //-- Hacer la transmision
  TXREG=car;
}


/*
****************************
*	Servicios del servidor *
****************************
*/

// Servicio PING
void serv_ping() {
  sci_write('o');
}


// Ajuste automatico del sensor
void serv_srf02_adjust() {
  srf02_adjust();
}


// muestra la version del Firmware
void serv_srf02_version() {
  unsigned char valor;
  
  sci_write('<');
  valor=srf02_read_version();
  sci_write( valor );
  sci_write('>');
}


// mira si es correcto el sensor
void serv_srf02_check() {
  unsigned char valor;

  valor=srf02_check();
  if (valor==0x80) {
    sci_write('1');
  } else {
    sci_write(valor);
  }
}


// Lanza una rafara para iniciar una medida
void serv_srf02_start() {
  srf02_start();
}


// Manda por el puerto serie el valor leido
void serv_srf02_read() {
  unsigned char valor_l, valor_h;
  
  valor_l=srf02_read_low();
  valor_h=srf02_read_high();
  sci_write(valor_h);
  sci_write(valor_l);

}

// Manda por el puerto serie el byte bajo de la lectura
void serv_srf02_read_low() {
  unsigned char valor_l;
  
  valor_l=srf02_read_low();
  sci_write(valor_l);

}


/*
******************************
*     FUNCIONES AUXILIARES   *
******************************
*/


/************************************************/
/* Hacer una pausa en unidades de 10ms          */
/* OJO: No es buena practica porque depende de  */
/* factores externos como optimizaci�n del      */
/* compilador o la propia frecuencia del micro  */
/*                                              */
/* Lo bueno es que una vez calibrada tenemos una*/
/* medida que no consume un TIMER               */
/************************************************/
void delay(int pausa)   
{
 unsigned int tmp;

  //-- Esperar hasta que trancurran pausa ticks de reloj
  while(pausa>0) {
    tmp=0xFFF;
  while (tmp>0) tmp--;
  pausa--;

  } 
}


/*
****************************************
*    FUNCIONES DE BAJO NIVEL DEL I2C   * 
****************************************
*/

// Envia Ack
// SDA = 0 
void i2c_SendAck() {
  ACKDT = 0;  // Establece un ACK
  ACKEN = 1;  // Lo envia
}


// Envia Nack para finalizar la recepecion
// SDA = 1
void i2c_SendNack() {
  ACKDT = 1;  // Establece un NACK
  ACKEN = 1;  // Lo envia
}


// verifica ACK
// Mira si en el 9 pulso de reloj (SCL en estado a 1) la se�al SDA est� a 0
unsigned char i2c_CheckACK() {
  if ( ACKSTAT == 0 ) {
    return 0;   // correcto (lo he recibido )
  } else { 
    return 1;	// incorrecto (no lo he recibido)
  }
}


// Envia la condicion de STOP
// Libera el BUS I2C, SDA y SCL a nivel alto
// SDA=1 cuando SCL=1. 
void i2c_SendStop() {
  PEN = 1;    // send stop bit
}


// Envia la condicion de START
// Inicializa el Bus I2C, SCL y SDA a nivel bajo
// Estando SCL=1 pone SDA=0, luego pone SCL=0
void i2c_SendStart() {
  SEN = 1;      // send start bit
}

// Espera a que el I2C reciba algun evento
void i2c_WaitMSSP() {
  while (SSPIF == 0);  // Espera evento
  SSPIF=0;             // Limpia FLAG
}

// Repeated start from master
// Queremos transmitir m�s datos pero sin dejar el BUS, es decir
// sin mandar previamente un STOP. O por ejemplo si queremos mandar un STOP y seguidamente un
// START para que ning�n otro dispositivo ocupe la l�nea usaremos esto.
void i2c_SendRStart() {
  RSEN=1;
}


// Leer Byte por el I2C
// Devuelve byte leido
unsigned char i2c_ReadByte() {
  RCEN=1;        // Activar el RCEN

  i2c_WaitMSSP();
  return SSPBUF;
}




// Envia un Dato por el Bus I2C
// Entrada el dato a enviar
void i2c_SendByte(unsigned char dato) {
  SSPBUF=dato;
}



// Fallo en el I2C
void i2c_fail() {
  i2c_SendStop();
  i2c_WaitMSSP();
  sci_write('f');   // @@@ Para depurar
}



/*
**************************************
*   FUNCIONES DE ALTO NIVEL del I2C  *
**************************************
*/

/* Configurar I2C
  Configuramos el I2C como Master
  y a una velocidad de 1Mhz
*/

void i2c_configure() {
  // configuramos SCL y SDA como pines de entrada
  TRISC=TRISC | 0x18;  // 1 entrada / 0 salida  
  
  SSPSTAT=0x0;  
  SSPSTAT=SSPSTAT | 0x80;  // SMP=1 Slew rate disable for 100Kh  @@@
                           // CKE=0 (modo maestro I2C) y UA=0 (7 bits)

  SSPCON=0x08;         // I2C master mode (uso la formula del reloj con SSPAD)  
  SSPCON=SSPCON | 0x20;  // enable I2C  SSPEN=1

  SSPCON2=0x00;   // no se usa por ahora
  // velocidad = FOSC / ( 4 * (sspad + 1) )
  // Fosc=20Mhz
  
  SSPADD=49;  // 49->100Khz  @@@
  //SSPADD=24; // 24 -> 400khz   @@@
  
  // Limpia Flags de eventos
  SSPIF=0;  //Limpia flag de eventos SSP 
}


// manda un byte al dispositivo i2c
unsigned char i2c_write_byte(unsigned char address, unsigned char reg, 
                             unsigned char dato)
{

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  
  
  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;  // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg);
  i2c_WaitMSSP(); 
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� mando el dato 
  i2c_SendByte(dato); 
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;    // Ha habido un error, salgo de la rutina
  }
  
  // 6� termino el envio
  i2c_SendStop();
  i2c_WaitMSSP();
  
  return 1;  // Byte mandado correctamente
}




// Lee 1 byte1  del dispositivo i2c
// El dato leido lo devuelve por el parametro dato
unsigned char i2c_read_byte(unsigned char address, unsigned char reg,
                            unsigned char *dato)
{

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg); 
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� Repeated start
  i2c_SendRStart();
  i2c_WaitMSSP();  

  // 4� mando direccion indicando lectura
  i2c_SendByte(address+1);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 5� leo el byte 
  *dato=i2c_ReadByte(); 
  
  // 6� Mando NACK
  i2c_SendNack();
  i2c_WaitMSSP();  
  
  // Mando el Stop
  i2c_SendStop();
  i2c_WaitMSSP();  
  
  return 1;   // Lectura correcta
}



/*
*******************************************
*  FUNCIONES DEL SENSOR DE ULTRASONIDOS   *   
*******************************************
*/


// Ajusta el sensor
void srf02_adjust() {
  i2c_write_byte(SRF02, 0, 0x60);
}



// Leer la version del Firmware
// En este sensor devuelve 0x05
unsigned char srf02_read_version() {
  unsigned char dato;
  
  i2c_read_byte(SRF02,0x00,&dato);
  
  return dato;
}


// En el registro 1 se debe leer 0x18
unsigned char srf02_check() {
  unsigned char dato;
  
  i2c_read_byte(SRF02,0x01,&dato);
  return dato;
}


// Iniciamos una medida en centimetros
void srf02_start() {
  i2c_write_byte(SRF02, 0, 0x51);
}

// Obtenemos el byte bajo de la lectura
unsigned char srf02_read_low() {
  unsigned char dato;
  
  i2c_read_byte(SRF02,0x03,&dato);
  
  return dato;
}

// Obtenemos el byte alto de la lectura
unsigned char srf02_read_high() {
  unsigned char dato;
  
  i2c_read_byte(SRF02,0x02,&dato);
  
  return dato;
}



//----------------------------
//- Comienzo del programa  
//----------------------------

void main(void)
{

  unsigned char tecla;
  
  //-- configura como salida el Puerto B excepto bit 0
  TRISB=0x01;
  PORTB=0x02;  // enciende el LED
  
  //-- Configurar el puerto serie
  sci_conf();
  
  //-- Configurar el I2C
  i2c_configure();
  
  //-- Hago una pausa antes de empezar
  delay(10);
  
  while (1) {
    tecla = sci_read();

    switch (tecla) {
    case 'p':   // servicio SPING
          serv_ping();
          break;
        
    case '1': // auto ajuste del sensor
          serv_srf02_adjust();
          break;
 
    case '2': // ver version
           serv_srf02_version();
           break;

    case '3': // revisar lectura del sensor
           serv_srf02_check();
           break;

    case '4': // Realizar una medida
          serv_srf02_start();
          delay(10);
          serv_srf02_read();
          break;
 
    case '5': // Realizar una medida
           serv_srf02_start();
           delay(10);
           serv_srf02_read_low();
           break;			 
 
    case 'z':   // enciendo el LED
            PORTB=PORTB | 0x02;
            break;
        
    case 'x':   // apago el LED
          PORTB=PORTB & 0xFD;
          break;

    default:  break;
  }
  //-- Cambiar el led de estado
  PORTB^=0x02;
  //-- Habria que meter un WDT
  delay(10);  // mientras tantos usamos una pausa
  }
} 
