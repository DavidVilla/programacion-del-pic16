#------------------------------------------------------------------------------
#-- Calcular el tiempo real que transcurre (en micro-segundos) cuando se 
#-- quiere realizar una pausa determinada con el timer 0
#-------------------------------------------------------------------------------
# ENTRADAS:
#-- FOSC: Frecuencia del cristal en Mhz
#-- Duracion: Duracion a conseguir (en micro-seg)
#-------------------------------------------------------------------------------
# DEVUELVE:
#-- Una lista con las duraciones correspondientes a los todos divisores posibles
#------------------------------------------------------------------------------
# EJEMPLO: calcular los tiempos reales que trancurren cuando se quiere hacer
#   una pausa de 1ms (1000 micro-segundos)
#
# octave> timer0_Duracion(20,1000)
# ans = NaN       NaN       NaN       NaN    998.40    998.40    998.40   1024.00
#
# Esto significa que hay 4 soluciones posibles, correspondientes a valores
# de los divisores de 32, 64, 128 y 256. Se observa que el tiempo real no llega
# a ser igual que el requerido, sino un poco mas pequeño o un poco mayor
#------------------------------------------------------------------------------
function Dur=timer0_Duracion(FOSC,Duracion)


#--- Divisores posibles para el Timer 0 
DIV=[2,4,8,16,32,64,128,256];

#-- Obtener la duracion en ticks
N=timer0_ticks(20,Duracion);

#-- Convertir los ticks a tiempo real en micro-segundos
Dur = N.*(4*DIV/FOSC);

return
