/*************************************************************************** */
/* sci-contador-eco.c                                                        */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de interrupciones en la recepcion. La rutina de atencion a la     */
/* interrupcion se encarga de hacer eco de todo lo recibido. El bucle        */
/* principal incrementa un contador que se muestra por el puerto B           */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

unsigned char c;

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/********************************************/
/* Enviar un byte por el SCI                */
/********************************************/
void sci_write(unsigned char dato)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (TXIF==0);
    
  //-- Hacer la transmision
  TXREG=dato;
}

/*****************************************/
/* Rutina de atencion a la interrupcion  */
/*****************************************/
void isr() interrupt 0 
{
  //-- Leer dato recibido
  c=RCREG;
  
  //-- Hacer eco
  sci_write(c);
}

/**********************************************/
/* Funcion de pausa                           */
/* ENTRADA: unidades de tiempo para la pausa  */
/**********************************************/
void pausa(unsigned char tiempo)
{
  unsigned char i;
  unsigned int j;
  int temp=0;

  //-- Se realizan dos bucles anidados
  for (i=0; i<tiempo; i++) {
    for (j=0; j<0x8000; j++) {
      //-- Operacion inutil, para consumir tiempo
      temp=temp+1;
    }
  }
}

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned char cont=0;

  //-- Configurar puerto B para salida
  TRISB=0;

  //-- Configurar puerto serie
  sci_conf();

  //-- Activar las interrupciones
  RCIE=1;
  PEIE=1;
  GIE=1;
  
  while(1) {
    PORTB=cont;
    pausa(4);
    cont++;
  }

}

