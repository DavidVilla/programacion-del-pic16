/*************************************************************************** */
/*  flancos2.c                                                               */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de deteccion de cambio en el pin RB7. Cada vez que ocurra un      */
/* flanco de subida o bajada por RB7, se cambiara el estado del led          */
/* La deteccion se realiza mediante interrupciones                           */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

volatile unsigned char temp;

void isr() interrupt 0 
{                                                                                                   /* rutina de servicio de interrupciones */
  //-- Para quitar flag hay que leer primero del puerto
  temp=PORTB;

  //-- Luego ponerlo a cero
  RBIF=0;

  //-- Cambiar el estado del led
  LED^=1;
}

void main(void)
{
  //-- Configurar RB7 para entrada y resto salidas
  TRISB=0x80;

  //-- Activar pull-ups del puerto B
  NOT_RBPU=0;

  //-- Habilitar interrupcion de cambio
  RBIE=1;

  //-- Habilitar las interrupciones en general
  GIE=1;

  while(1) {
  }

}

