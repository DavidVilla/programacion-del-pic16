/*************************************************************************** */
/* timer0-pausa10ms.c                                                        */
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de realizacion de una pausa de 10ms. Se hace parpadear el LED     */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valor inicial del TMR0 para realizar una pausa de 10ms
//-- Divisor del prescar a 256
#define T0INI 61

#define LED RB1

/******************************/
/* Hacer una pausa de 10ms    */
/******************************/
void pausa_10ms()
{
  //-- Dar valor inicial del timer
  TMR0=T0INI;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);

}

void main(void)
{
  TRISB1=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 256
  PS2=1; PS1=1; PS0=1;

  while(1) {
    pausa_10ms();
    LED=1;
    pausa_10ms();
    LED=0;
  }
}

