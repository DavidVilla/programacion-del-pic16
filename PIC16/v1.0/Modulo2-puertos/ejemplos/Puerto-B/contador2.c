/*************************************************************************** */
/* contador2.c                                                               */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Contador desde 0 hasta 128 que se muestra por los leds conectados al     */
/* puerto B... Se ha incluido una rutina de pausa por lo que ahora si        */
/* podemos ver como cambian los leds                                         */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define PUERTOB_SALIDA TRISB=0x00

/**********************************************/
/* Funcion de pausa                           */
/* ENTRADA: unidades de tiempo para la pausa  */
/**********************************************/
void pausa(unsigned char tiempo)
{
  unsigned char i;
  unsigned int j;
  int temp=0;

  //-- Se realizan dos bucles anidados
  for (i=0; i<tiempo; i++) {
    for (j=0; j<0x8000; j++) {
      //-- Operacion inutil, para consumir tiempo
      temp=temp+1;
    }
  }
}

void main(void)
{
  unsigned char i;
  
  //-- Configurar puerto B como salida
  PUERTOB_SALIDA;

  //-- Bucle desde i=0 hasta 100. La variable i toma los valores
  //-- 0, 1, 2, 3.....
  for (i=0; i<=128; i++) {
    PORTB=i;  //-- Visualizar la variable i en los leds
    pausa(1); //-- Hacer una pausa
  }

  //-- Terminar
  while(1);

}

