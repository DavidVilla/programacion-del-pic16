/*************************************************************************** */
/* timer0-pwm1.c                                                             */  
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de generacion de una senal PWM de ciclos de trabajo del 20 y 50% */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/


#include <pic16f876a.h>

//-- Valores iniciales del temporizador
#define TON_C20  217
#define TOFF_C20 100
#define TON_C50  158
#define TOFF_C50 158

//-- Tablas que contiene los tiempos de on y off de las dos señales PWM
//-- a generar
unsigned char Ton[]= {TON_C20,  TON_C50};
unsigned char Toff[]={TOFF_C20, TOFF_C50};

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/***************************************************/
/* Generar la senal pwm indicada  por el indice i  */
/* Se realizan varios ciclos para poder verla      */
/* funcionar en los leds                           */
/***************************************************/
void senal(unsigned char i)
{
  unsigned int ciclos;

  for (ciclos=0; ciclos<100; ciclos++) {
    PORTB=0xFF;
    timer0_delay(Ton[i]);
    PORTB=0x00;
    timer0_delay(Toff[i]);
  }
}

void main(void)
{
  TRISB=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler A 256
  PS2=1; PS1=1; PS0=1;

  while(1) {
    senal(0);  //-- ciclo del 20%
    senal(1);  //-- ciclo del 50%
  }   
}

