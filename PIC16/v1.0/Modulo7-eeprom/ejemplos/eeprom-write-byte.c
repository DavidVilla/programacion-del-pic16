/*************************************************************************** */
/* eeprom-write-byte.c                                                       */
/*---------------------------------------------------------------------------*/
/* MEMORIA EEPROM                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de escritura de un byte en la memoria eeprom                      */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

/**********************************************/
/* Escribir un byte en la direccion indicada  */
/**********************************************/
void eeprom_write(unsigned char dir, unsigned char dato)
{
  EEADR=dir;     //-- Establecer direccion
  EEDATA=dato;   //-- Establecer el dato
  EEPGD=0;       //-- Indicar acceso a la memoria eeprom

  WREN=1;
  EECON2=0x55;
  EECON2=0xAA;
  WR=1;       //-- Comenzar la escritura!

  //-- Esperar a que la escritura termine
  while (EEIF==0);

  //-- Quitar flag de interrupcion
  EEIF=0;
}


void main(void)
{
  TRISB1=0;

  //-- Escribir el byte 0x55 en la direccion 0
  eeprom_write(0, 0x55);

  //-- Encender led para indicar que escritura completa
  RB1=1;

  while(1);
}
