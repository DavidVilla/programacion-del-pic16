#------------------------------------------------------------------------------
#-- Juan Gonzalez Gomez. Abril 2009. LICENCIA GPL                  
#------------------------------------------------------------------------------
#-- Script para usar con los microcontroladores PIC16F87x                    
#------------------------------------------------------------------------------
#-- Calcular el valor del registro TMR0 y de su prescaler para configurarlo
#-- para generar una senal cuadrada de la frecuencia especificada
#-- ENTRADAS: 
#--    -FOSC : Frecuencia del cristal empleado. En Mhz. Para la skypic: FOSC=20
#--    -Frecuencia: Frecuencia deseada, en Hz
#-----------------------------------------------------------------------------
#-- EJEMPLO: Generar un tono de 800Hz con la Skypic usando el timer 0
#--
#-- octave> timer0_TMR0(20,800);
# TMR0= 61, PS2:0= 011, Div=  16, Frec= 801.3 Hz, Error= -1.28 Hz
# TMR0= 158, PS2:0= 100, Div=  32, Frec= 797.2 Hz, Error= 2.81 Hz
# TMR0= 207, PS2:0= 101, Div=  64, Frec= 797.2 Hz, Error= 2.81 Hz
# TMR0= 232, PS2:0= 110, Div= 128, Frec= 813.8 Hz, Error= -13.80 Hz
# TMR0= 244, PS2:0= 111, Div= 256, Frec= 813.8 Hz, Error= -13.80 Hz
#
# Esto significa que hay 5 soluciones posibles, usando los divisores
# de 16, 32, 64, 128 y 256. La frecuencia real no llega a ser de 800Hz
#------------------------------------------------------------------------------

function TMR0=timer0_TMR0_frec(FOSC,frecuencia)

#-- Valores posibles de los divisores
DIV=[2,4,8,16,32,64,128,256];

#-- Valores del prescaler asociados a cada uno de los divisores
PS=[000,001,010,011,100,101,110,111];

#-- Calcular el valor del semiperiodo en Hz
Duracion = 1/(2*frecuencia)*1000000;

#-- Calcular los valores de TMR0 para todos los divisores
TMR0=256-timer0_ticks(FOSC,Duracion);

#-- Calcular la frecuencia real
frec = 1000000./(2*timer0_Duracion(FOSC,Duracion));

#-- Calcular el error como la frecuencia deseada menos la frecuencia real
Error = frecuencia-frec;

#-- Mostrar los resultados
#-- Solo se sacan aquellos valores de TMR0 menores a 256
for i=1:length(TMR0)
  if TMR0(i)<256 && TMR0(i)>=0
    printf ("TMR0= %d, PS2:0= %03d, Div= %3d, Frec= %.1f Hz, Error= %.2f Hz\n",
             TMR0(i),PS(i),DIV(i), frec(i),Error(i));
  endif;           
endfor


return
