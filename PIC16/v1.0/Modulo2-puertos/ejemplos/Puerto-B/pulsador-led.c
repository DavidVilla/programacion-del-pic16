/*************************************************************************** */
/* pulsador-led.c                                                            */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de entrada y salida. Se configura el pin RB0 como entrada y       */
/* RB1 como salida. Por RB1 (el led) se saca el valor negado de lo recibido  */
/* por RB0 (pulsador)                                                        */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void main(void)
{
  //-- Configurar RB1 somo salida (LED) y RB0 (pulsador) como entrada
  TRISB0=1;  //-- En realidad, los bits de TRISB estan ya a '1' por defecto
  TRISB1=0;

  while(1) {
    if (RB0==1) {
      RB1=0;
    } 
    else {
      RB1=1;
    }
  }

}

