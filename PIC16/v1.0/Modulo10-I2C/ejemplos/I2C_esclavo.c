/*******************************************************/
/* M�dulo esclavo I2C                       junio 2009 */
/*-----------------------------------------------------*/
/* Protocolo del esclavo                               */
/*    Direcci�n 0xA0                                   */
/*                                                     */
/* El esclavo tiene 10 registros de escritura/lectura  */
/*                                                     */
/*                    Escritura            Lectura     */
/*    Registro 0 :    0 apaga led          estado      */
/*                    1 enciende led                   */ 
/*                                                     */
/*    Registro 2 :    ---             Estado Pulsador  */
/*                                                     */
/*    Registro 5 :    manda valor por      estado      */               
/*                    puerto serie                     */
/*                                                     */
/* Resto registros       estado            estado      */
/* 1,3,4,6,7,8,9                                       */
/*                                                     */
/* Si se emplean funciones de escribir o leer mas de   */
/* byte, el esclavo incrementar� el indice del ultimo  */
/* registro automaticamente, y en caso de llegar al 10 */
/* continuara por el primero. Es decir, si hacemos una */
/* lectura de 10 bytes empezando por el registro 0,    */
/* leeremos toda la memoria                            */
/*******************************************************/

//-- Especificar el pic a emplear
#include <pic16f876a.h>


//-- Definiciones relacionadas con la SkyPic
#define LED      RB1     // Pin del led de la Skypic
#define PULSADOR RB0     // Pin del pulsador en la Skypic
#define APRETADO   0     //-- Cuando pulsador apretado RB0 se pone a 0

//-- Define la direccion del Esclavo
// Direcci�n del esclavo en el I2C OJO, siempre tiene que terminar en CERO
// (son de 7 bits y el octavo es 0)
#define DIR_I2C   0xA0

//-- Me declaro un Buffer que contiene los 10 registros 
//-- que usara el esclavo por I2C
#define  MAX_SIZE  10

unsigned char slave_registers[MAX_SIZE];
unsigned char reg_index;   // puntero para recorrer la tabla


/**********************************************/
/* Libreria Serie                             */
/**********************************************/


// Configurar el puerto serie a N81 y 9600 Baudios  */
void sci_conf(void)
{
  SPBRG = 0x81;  //-- 9600 baudios (con cristal de 20MHz)
  TXSTA = 0x24;  //-- Configurar transmisor
  RCSTA = 0x90;  //-- Configurar receptor
}


// Recibir un caracter por el SCI        
// DEVUELVE: Caracter recibido            
unsigned char sci_read(void)
{
  //-- Eserar hasta que llegue el dato
  while (!RCIF);
  return RCREG;
}


// Transmitir un caracter por el SCI    
// ENTRADAS: Caracter a enviar            

void sci_write(unsigned char car)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (!TXIF);
    
  //-- Hacer la transmision
  TXREG=car;
}


/*
****************************************
*    I2C como esclavo                  *
****************************************
*/

// Envia un Dato por el Bus I2C
// Entrada el dato a enviar
void i2c_slave_SendByte(unsigned char dato)
{
  while (BF==1) ; // espera a que el buffer este libre

bucle:          // No es lo mas elegante pero vale.
  WCOL=0;         // Borramos el bit de colision
  SSPBUF=dato;    // manda el dato
  if (WCOL==1) {  // se ha producido colision, reintentar
    goto bucle; 
  }
  CKP=1;          // libera el reloj
}


void i2c_slave_configure(unsigned char dir)
{
  // configuramos SCL y SDA como pines de entrada
  TRISC=TRISC | 0x18;  // 1 entrada / 0 salida  
  
  SSPSTAT=0x0;  
  SSPSTAT=SSPSTAT | 0x80;  // SMP=1 Slew rate disable for 100Kh
                           // CKE=0  (no SMBus) y UA=0 (7 bits)
  // I2C slave mode (SSPADD almacena la direccion), CKP=1 (release SCL)
  SSPCON=0x16;   

  // Aqui coloco la direccion del esclavo
  SSPADD=dir; 

  SSPCON2=0x80;   // Permito interrupciones cuando se detecfta una direccion en el esclavo I2C

  // Activo el I2C
  SSPCON=SSPCON | 0x20;  // enable I2C  SSPEN=1
  
  // Limpia Flags de eventos
  SSPIE=1;  //PIE1: Activo las interrupciones del I2C 
  SSPIF=0;  //Limpia flag de eventos SSP 
}



void isr() interrupt 0
{

  volatile unsigned char tmp;  // importante para que no optimice el compilador
  static unsigned char addr_flag;

  if (SSPIF==1) {  // La interrupci�n se ha producido por el I2C

    tmp=SSPSTAT & 0x2D;   // Me quedo solo con las posibles causas de interrupcion (

    // Estado 1: I2c write operation -> last byte was an address byte
    // Se ha recibido una direccion en modo lectura
    // SSPSTAT : S=1, D_A=0, R_W=0, BF=1
    if (tmp==0x09) {
      tmp=SSPBUF;   // Lectura de la direcci�n, pero se descarta
      addr_flag=1;
    }
    // Estado 2: I2c write operation -> last byte was a data byte
    // Se ha recibido un dato. Generalmente el primero es la direcci�n del registro
    // y los siguientes los datos a guardar a partir del registro.
    // SSPSTAT : S=1, D_A=1, R_W=0, BF=1
    else if (tmp==0x29) {
      if (addr_flag==1) {
        reg_index=SSPBUF;   // El primer dato es la direccion del registro
        if (reg_index > MAX_SIZE) {
          reg_index=MAX_SIZE;
        }
        addr_flag=2;
      } 
      else if (addr_flag==2) {  // Los siguientes valores se guardan consecutivamente
        slave_registers[reg_index++]=SSPBUF;
        if (reg_index>MAX_SIZE) {
          reg_index=0;
        }
      }
    }

    // Estado 3: I2c read operation -> last byte was an address byte
    // Devolvemos el dato que se ha pedido
    // SSPSTAT : S=1, D_A=0, R_W=1
    else if ( (tmp&0xFE)==0x0C) {
      i2c_slave_SendByte(slave_registers[reg_index]); 
    }
    // Estado 4: I2c read operation -> last byte was an data byte
    // Se produce cuando queremos leer mas de un dato
    // SSPSTAT : S=1, D_A=1, R_W=1, BF=0
    else if (tmp==0x2C) {
      reg_index++;
      if (reg_index> MAX_SIZE) {
        reg_index=0;
      }
      i2c_slave_SendByte(slave_registers[reg_index]);   // Mando el dato
    }

    /* SON ESTADOS LOGICOS PERO SIN ACCION (los dejamos comentados)
    // Estado 5: Logic reset by NACK from MASTER
    // SSPSTAT : S=1, D_A=1, BF=0 , CKP=1
    else if ( ((tmp&0xFB)==0x28) && (CKP==1)) {
      // El maestro no quiere mas datos
    }
    // Error. 
    else {
        //sci_write('E');
    }
    */
    SSPIF=0;  // Borra el FLAG de interrupcion
  }
}


//----------------------------
//- Comienzo del programa  
//----------------------------

void main(void)
{

  unsigned char last_value;

  //-- Puerto B de salida: Datos, menos el primer bit
  TRISB=0x01;
  //-- Puerto A: Bits RB0, RB1 y RB2 de salida, resto entradas
  TRISA=0xF8;
  //-- Configurar el puerto A como digital
  ADCON1=0x06;

  sci_conf();

  // Configura los registros del esclavo
  slave_registers[0]='0';
  slave_registers[1]='1';
  slave_registers[2]='2';
  slave_registers[3]='3';
  slave_registers[4]='4';
  slave_registers[5]='5';
  slave_registers[6]='6';
  slave_registers[7]='7';
  slave_registers[8]='8';
  slave_registers[9]='9';

  reg_index=0;
  last_value=0;

  //-- Configurar el I2C como esclavo, le paso la direccion como argumento
  i2c_slave_configure(DIR_I2C);
  
  //-- Activar interrupciones
  PEIE=1; // las de los perifiericos
  GIE=1;  // las globales

  while (1) {

    // Miro si he recibido la se�al para encender o apagar el LED
    if ( slave_registers[0]==0 ) {
      LED=0;
    }
    if ( slave_registers[0]==1) {
      LED=1;
    }

    // Saco el �ltimo dato recibido por el puerto serie
    if (last_value!=slave_registers[5]) {
      sci_write(slave_registers[5]);
      last_value=slave_registers[5];
    }

    // Guardo el estado del pulsador en el registro
    if (PULSADOR==APRETADO) {
      sci_write('*');
      slave_registers[2]=1;
    } else {
      slave_registers[2]=0;
    }
  }
} 
