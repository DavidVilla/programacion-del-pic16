/*************************************************************************** */
/* timer0-pwm2.c                                                             */  
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de generacion de una senal PWM de ciclos de trabajo del 20 y 50% */
/*  Es un ejemplo mejorado que solo depende de T y de Ton. No hace falta     */
/*  definir una tabla con los pares de valores Ton y Toff                    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/


#include <pic16f876a.h>

//-- Periodo del PWM en ticks
//-- Prescaler a 256
#define T 195

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/***************************************************/
/* Generar la senal pwm indicada  por el indice i  */
/* Se realizan varios ciclos para poder verla      */
/* funcionar en los leds                           */
/***************************************************/
void senal(unsigned char Ton)
{
  unsigned int ciclos;

  for (ciclos=0; ciclos<100; ciclos++) {
    PORTB=0xFF;
    timer0_delay(255-Ton);
    PORTB=0x00;
    timer0_delay(255-T+Ton);
  }
}

void main(void)
{
  TRISB=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler A 256
  PS2=1; PS1=1; PS0=1;

  while(1) {
    senal(39);  //-- ciclo del 20%
    senal(97);  //-- ciclo del 50%
  }   
}

