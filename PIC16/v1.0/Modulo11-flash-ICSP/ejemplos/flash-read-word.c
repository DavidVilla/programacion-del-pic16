/*************************************************************************** */
/* flash-read-word.c                                                         */
/*---------------------------------------------------------------------------*/
/* MEMORIA FLASH                                                             */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de lectura de la memoria flash                                    */
/* Se lee una palabra y se envia por el sci                                  */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

#define LED RB1

//---------------------------------------------------------
//-- Leer de la memoria flash
//---------------------------------------------------------
//-- ENTRADAS: 
//--   dir: Direccion de la flash donde leer
//-- SALIDAS:
//--   Devuelve la palabra (14 bits) que esta en esa posicion
//-----------------------------------------------------------
unsigned int flash_read(unsigned int dir)
{
  //-- Establecer la direccion
  EEADRH = dir>>8;
  EEADR = dir&0xFF;

  //-- Seleccionar memoria flash
  EEPGD=1;

  //-- Establecer bit de lectura
  RD=1;

  //-- Esperar ciclos basura...
  RD=1;
  RD=1;

  return EEDATH<<8 | EEDATA;
}

void main(void)
{
  unsigned int dato;
  unsigned char i;
    

  //-- Configurar led de la Skypic
  TRISB1 = 0;

  //-- Inicializar puerto serie
  sci_conf();

  //-- Leer las 4 palabras situadas a partir de la direccion 0x800
  //-- y enviarlas por el puerto serie
  for (i=0; i<4; i++) {
    //-- Leer palabra de la memoria flash
    dato=flash_read(0x0800+i);
   
    //-- Enviarla por el sci
    sci_hexint(dato);
    sci_write(' ');
  }

  //-- Indicar que lectura finalizada
  LED=1;
    
  while(1);
}


