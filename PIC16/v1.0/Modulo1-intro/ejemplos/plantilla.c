/*************************************************************************** */
/* plantilla.c                                                               */
/*---------------------------------------------------------------------------*/
/* Plantilla generica para escribir programas en C con el SDCC               */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

//-- Cabecera del PIC a emplear
#include <pic16f876a.h>


// Bits de configuracion (opcional)
typedef unsigned int word;
word at 0x2007 CONFIG = 
  _RC_OSC & 
  _WDT_ON & 
  _PWRTE_OFF & 
  _BODEN_ON & 
  _LVP_ON & 
  _CPD_OFF & 
  _WRT_OFF & 
  _DEBUG_OFF &
  _CP_OFF;

/*****************************************/
/* Rutina de atencion a la interrupcion  */
/* (opcional)                            */
/*****************************************/
void isr() interrupt 0 {
    
}

/*****************************/
/* Programa principal        */
/*****************************/
void main() {
    
}
