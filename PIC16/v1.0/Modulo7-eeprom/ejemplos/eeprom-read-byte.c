/*************************************************************************** */
/* eeprom-read-byte.c                                                        */
/*---------------------------------------------------------------------------*/
/* MEMORIA EEPROM                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de lectura de la memoria eeprom. Se lee el byte de la direccion   */
/* 0 y se saca por los leds                                                  */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

unsigned char eeprom_read(unsigned char dir)
{
  EEADR=dir;  //-- Direccion a la que acceder
  EEPGD=0;
  RD=1;   //-- comenzar la lectura
  return EEDATA;
}


void main(void)
{
  TRISB=0;

  PORTB=eeprom_read(0x00);

  while(1);
}
