/*************************************************************************** */
/*  pull-ups.c                                                               */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de activacion de las resistencias del pull-up del puerto B        */
/* El programa muestra por el LED el estado del pin de entrada RB7           */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

void main(void)
{
  //-- Configurar RB1 (led) para salida, resto entradas
  TRISB1=0;

  //-- Activar pull-ups del puerto B
  NOT_RBPU=0;

  while(1) {
    //-- Mostrar por el LED el estado del pin RB7
    LED=RB7;
  }

}

