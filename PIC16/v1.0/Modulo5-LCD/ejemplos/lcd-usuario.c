/*************************************************************************** */
/* lcd-usuario.c                                                             */ 
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* CONTROL DE UN LCD                                                         */
/*---------------------------------------------------------------------------*/
/* Ejemplo de como definir nuestros propios caracteres para ser impresos en  */
/* el LCD                                                                    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Pines de conexionado para el LCD
#define RW  RA2
#define RS  RA1
#define E   RA0

//-- Definicion de los comandos del LCD
#define INIT     0x38
#define ENCENDER 0x0E
#define HOME     0x02
#define CLS      0x01

//-- Valor inicial del timer0 para hacer una pausa de 10ms
//-- Divisor del prescar a 256
#define T_10ms 61

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/*******************************************************/
/* Pausa                                               */
/* ENTRADA: duracion de la pausa en centesimas (10ms)  */
/*******************************************************/
void delay(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}

/*********************************************/
/* Habilitar el dato escrito en el puerto B  */
/*********************************************/
void enable()
{
  //-- Hay que garantizar un tiempo minimo de 500ns
  //-- A 20Mhz, las instrucciones tardan 200ns.
  //-- Por eso nos curamos en salud y repetimos la instruccion
  //-- tres veces. 
  E=1; E=1; E=1;
  E=0; E=0; E=0;
}

/**********************************/
/* Enviar un comando al LCD       */
/**********************************/
void lcd_cmd(unsigned char cmd)
{
  RS=0;
  PORTB=cmd;
  enable();
  delay(1);
}

/*********************************/
/* Escribir un dato en el lcd    */
/*********************************/
void lcd_write(unsigned char car)
{
  RS=1;
  PORTB=car;
  enable(); 
  delay(1);
}

void main(void)
{

  //-- Configurar el lcd
  //-- Puerto B de salida: Datos
  TRISB=0x00;
  //-- Puerto A: Bits RB0, RB1 y RB2 de salida, resto entradas
  TRISA=0x0F8;
  //-- Configurar el puerto A como digital
  ADCON1=0x06;

  //-- Configurar temporizador para hacer pausas
  T0CS=0; PSA=0;
  //-- Configurar el prescaler (division entre 64)
  PS2=1; PS1=1; PS0=1;

  //-- Pausa inicial
  delay(1);

  //-- Inicializar lcd (para trabajar a 8 bits)
  RW=0; E=0;
  lcd_cmd(INIT);

  //--  Modo de funcionamiento
  lcd_cmd(0x06);

  //-- Encender el LCD
  lcd_cmd(ENCENDER);

  //-- Cursor a HOME
  lcd_cmd(HOME);

  //-- CLS
  lcd_cmd(CLS);

  //-- Definir el caracter 0: Monigote
  lcd_cmd(0x40);
  lcd_write(0x0E);
  lcd_write(0x0E);
  lcd_write(0x0E);
  lcd_write(0x04);
  lcd_write(0x1F);
  lcd_write(0x04);
  lcd_write(0x0A);
  lcd_write(0x11);

  //-- Definir caracter 1: Llave 
  lcd_write(0x0E);
  lcd_write(0x11);
  lcd_write(0x0E);
  lcd_write(0x04);
  lcd_write(0x04);
  lcd_write(0x0C);
  lcd_write(0x04);
  lcd_write(0x0C);

  //-- Definir el caracter 2: Comecocos abierto
  lcd_write(0x07);
  lcd_write(0x0B);
  lcd_write(0x0E);
  lcd_write(0x1C);
  lcd_write(0x18);
  lcd_write(0x1C);
  lcd_write(0x0E);
  lcd_write(0x07);

  //-- Definir caracter 3: Comecocos cerrado
  lcd_write(0x00);
  lcd_write(0x06);
  lcd_write(0x0B);
  lcd_write(0x1F);
  lcd_write(0x1F);
  lcd_write(0x0F);
  lcd_write(0x06);
  lcd_write(0x00);


  //-- Escribir los caracteres definidos
  lcd_cmd(0x80);
  lcd_write(0);
  lcd_write(1);
  lcd_write(2);
  lcd_write(3);
 
  
  while(1);
}



