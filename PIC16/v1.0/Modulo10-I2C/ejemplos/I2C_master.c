/*******************************************************/
/* M�dulo esclavo I2C                       junio 2009 */
/*-----------------------------------------------------*/
/* Protocolo del esclavo                               */
/*    Direcci�n 0xA0                                   */
/*                                                     */
/* El esclavo tiene 10 registros de escritura/lectura  */
/*                                                     */
/*                    Escritura            Lectura     */
/*    Registro 0 :    0 apaga led          estado      */
/*                    1 enciende led                   */ 
/*                                                     */
/*    Registro 2 :    ---             Estado Pulsador  */
/*                                                     */
/*    Registro 5 :    manda valor por      estado      */               
/*                    puerto serie                     */
/*                                                     */
/* Resto registros       estado            estado      */
/* 1,3,4,6,7,8,9                                       */
/*                                                     */
/* Si se emplean funciones de escribir o leer mas de   */
/* byte, el esclavo incrementar� el indice del ultimo  */
/* registro automaticamente, y en caso de llegar al 10 */
/* continuara por el primero. Es decir, si hacemos una */
/* lectura de 10 bytes empezando por el registro 0,    */
/* leeremos toda la memoria                            */
/*******************************************************/



//-- Especificar el pic a emplear
#include <pic16f876a.h>


//-- Definiciones relacionadas con la SkyPic
#define LED   RB1     // Pin del led de la Skypic

// Direcci�n del esclavo en el I2C OJO, siempre tiene que terminar en CERO 
//(son de 7 bits y el octavo es 0)
#define DIR_I2C   0xA0     

//-- Valor inicial del timer0 para hacer una pausa de 10ms
//-- Divisor del prescar a 256
#define T_10ms 61


/*
******************************
*     FUNCIONES AUXILIARES   *
******************************
*/


void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}


/*******************************************************/
/* Pausa                                               */
/* ENTRADA: duracion de la pausa en centesimas (10ms)  */
/*******************************************************/
void delay(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}

/*
****************************************
*    FUNCIONES DE BAJO NIVEL DEL I2C   * 
****************************************
*/

// Envia Ack
// SDA = 0 
void i2c_SendAck()
{
  ACKDT = 0;  // Establece un ACK
  ACKEN = 1;  // Lo envia
}


// Envia Nack para finalizar la recepecion
// SDA = 1
void i2c_SendNack()
{
  ACKDT = 1;  // Establece un NACK
  ACKEN = 1;  // Lo envia
}


// verifica ACK
// Mira si en el 9 pulso de reloj (SCL en estado a 1) la se�al SDA est� a 0
unsigned char i2c_CheckACK()
{
  if ( ACKSTAT == 0 ) {
    return 0;   // correcto (lo he recibido )
  } else { 
    return 1;	// incorrecto (no lo he recibido)
  }
}


// Envia la condicion de STOP
// Libera el BUS I2C, SDA y SCL a nivel alto
// SDA=1 cuando SCL=1. 
void i2c_SendStop() {
  PEN = 1;    // send stop bit
}


// Envia la condicion de START
// Inicializa el Bus I2C, SCL y SDA a nivel bajo
// Estando SCL=1 pone SDA=0, luego pone SCL=0
void i2c_SendStart()
{
  SEN = 1;	    // send start bit
}

// Espera a que el I2C reciba algun evento
void i2c_WaitMSSP() 
{
  while (SSPIF == 0);  // Espera evento
  SSPIF=0;             // Limpia FLAG
}


// Espera a que el I2C reciba algun evento
unsigned char i2c_MSSP_Status()
{
  return SSPIF; 
}




// Repeated start from master
// Queremos transmitir m�s datos pero sin dejar el BUS, es decir
// sin mandar previamente un STOP. O por ejemplo si queremos mandar un STOP y seguidamente un
// START para que ning�n otro dispositivo ocupe la l�nea usaremos esto.
void i2c_SendRStart() {
  RSEN=1;
}


// Leer Byte por el I2C
// Devuelve byte leido
unsigned char i2c_ReadByte() {
  RCEN=1;        // Activar el RCEN

  i2c_WaitMSSP();
  return SSPBUF;
}




// Envia un Dato por el Bus I2C
// Entrada el dato a enviar
void i2c_SendByte(unsigned char dato) {
  SSPBUF=dato;
}



// Fallo en el I2C
void i2c_fail() {
  i2c_SendStop();
  i2c_WaitMSSP();
}



/*
**************************************
*   FUNCIONES DE ALTO NIVEL del I2C  *
**************************************
*/


/* Configurar I2C
  Configuramos el I2C como Master
  y a una velocidad de 1Mhz
*/

void i2c_configure()
{
  // configuramos SCL y SDA como pines de entrada
  TRISC=TRISC | 0x18;  // 1 entrada / 0 salida  
  
  SSPSTAT=0x0;  
  SSPSTAT=SSPSTAT | 0x80;  // SMP=1 Slew rate disable for 100Kh  @@@
                           // CKE=0 (modo maestro I2C) y UA=0 (7 bits)

  SSPCON=0x08;       // I2C master mode (uso la formula del reloj con SSPAD)  
  SSPCON=SSPCON | 0x20;  // enable I2C  SSPEN=1

  SSPCON2=0x00;   // no se usa por ahora
  // velocidad = FOSC / ( 4 * (sspad + 1) )
  // Fosc=20Mhz

  SSPADD=49;  // 49->100Khz  @@@
  //SSPADD=24; // 24 -> 400khz   @@@

  // Limpia Flags de eventos
  SSPIF=0;  //Limpia flag de eventos SSP 
}



// manda un byte al dispositivo i2c
unsigned char i2c_write_byte(unsigned char address, unsigned char reg,
                             unsigned char dato)
{
  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;  // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg);
  i2c_WaitMSSP(); 
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� mando el dato 
  i2c_SendByte(dato); 
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;    // Ha habido un error, salgo de la rutina
  }

  // 6� termino el envio
  i2c_SendStop();
  i2c_WaitMSSP();

  return 1;  // Byte mandado correctamente
}


// Lee 1 byte1  del dispositivo i2c
// El dato leido lo devuelve por el parametro dato
unsigned char i2c_read_byte(unsigned char address, unsigned char reg, 
                            unsigned char *dato)
{

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg); 
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� Repeated start
  i2c_SendRStart();
  i2c_WaitMSSP();  

  // 4� mando direccion indicando lectura
  i2c_SendByte(address+1);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 5� leo el byte 
  *dato=i2c_ReadByte(); 

  // 6� Mando NACK
  i2c_SendNack();
  i2c_WaitMSSP();  

  // Mando el Stop
  i2c_SendStop();
  i2c_WaitMSSP();  

  return 1;   // Lectura correcta

}

// Lee 2 bytes consecutivos  del dispositivo i2c
// El dato leido lo devuelve por el parametro dato
unsigned char i2c_read_word(unsigned char address, unsigned char reg, 
                            unsigned int *dato)
{
  unsigned char tmp_H, tmp_L;

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  
    
  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg); 
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� Repeated start
  i2c_SendRStart();
  i2c_WaitMSSP();  

  // 4� mando direccion indicando lectura
  i2c_SendByte(address+1);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 5� leo el 1 byte 
  tmp_H=i2c_ReadByte(); 
  
  // 6� Mando ACK
  i2c_SendAck();
  i2c_WaitMSSP();  
  
  // 7� leo el 2 byte 
  tmp_L=i2c_ReadByte(); 
  
  // 6� Mando NACK
  i2c_SendNack();
  i2c_WaitMSSP();  

  // Mando el Stop
  i2c_SendStop();
  i2c_WaitMSSP();  
  
  *dato=(tmp_H*256) + tmp_L;
  
  return 1;   // Lectura correcta
}


//----------------------------
//- Comienzo del programa  
//----------------------------

void main(void)
{
  unsigned char ret;
  unsigned char dato;

  //-- Puerto B de salida: Datos
  TRISB=0x00;
  //-- Puerto A: Bits RB0, RB1 y RB2 de salida, resto entradas
  TRISA=0x0F8;
  //-- Configurar el puerto A como digital
  ADCON1=0x06;

  //-- Configurar temporizador para hacer pausas
  T0CS=0; PSA=0;
  //-- Configurar el prescaler (division entre 64)
  PS2=1; PS1=1; PS0=1;

  //-- Pausa inicial
  delay(100);
  
  //-- Configurar el I2C como master
  i2c_configure();

  //-- Hago una pausa antes de empezar
  delay(10);
  
  while (1) {
    // ACtivo el LED del esclavo
    i2c_write_byte(DIR_I2C, 0x0, 1);
    // Mando una 'A' por el puerto serie del esclavo
    i2c_write_byte(DIR_I2C, 0x5, 'A');
    delay(10);

    // desactivo el LED del esclavo
    i2c_write_byte(DIR_I2C, 0x0, 0);
    // Mando una 'B' por el puerto serie del esclavo
    i2c_write_byte(DIR_I2C, 0x5, 'B');
    delay(10);  

    // Leo el registro que me indica el estado del pulsador del esclavo
    ret=i2c_read_byte(DIR_I2C, 0x02, &dato);
    if (ret==1) {
      if (dato==0x01) {
        LED=1;
      }
      else {
        LED=0;
      }
    }
  }
} 
