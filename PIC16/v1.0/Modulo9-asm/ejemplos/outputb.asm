;****************************************************************************
;  outputb.asm                                                              *
;---------------------------------------------------------------------------*
; Ejemplo para la tarjeta SKYPIC                                            *
;---------------------------------------------------------------------------*
;  Enviar un VALOR al puerto B                                              *
;---------------------------------------------------------------------------*
;  LICENCIA GPL                                                             *
;****************************************************************************

; --- Especificar el PIC a emplear
  INCLUDE "p16f876a.inc"

;-- Definiciones
#define VALOR 0xAA

   ORG 0

  ;-- Codigo de inicializacion para bootloader
  CLRF    STATUS
  MOVLW   0
  MOVWF   PCLATH
  GOTO    start
   
   
start
;-- Configurar puerto B para salida
  BSF  STATUS,RP0 ; Cambiar al banco 1
  CLRF TRISB      ; Poner todos los bits a 0 (salidas)

;-- Enviar un valor por el puerto B
  BCF STATUS,RP0  ; Cambiar al banco 0
  MOVLW VALOR
  MOVWF PORTB     ; VALOR-->PUERTO B


fin goto fin
 END
