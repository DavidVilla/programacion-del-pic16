/*************************************************************************** */
/* timer0-escala.c                                                           */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* TIMER0. Ejemplo de generacion de notas musicales usando ondas cuadradas   */
/* Las senales se generan en el pin RB1, donde esta situado el led de la     */
/* Skypic. Si se coloca un pequeño altavoz se podrá oir la escala musical    */
/*---------------------------------------------------------------------------*/
/* Las frecuencias de las notas estan dadas por la siguiente tabla:          */
/*  Nota  Frecuencia (Hz)                                                    */
/*  DO       261                                                             */
/*  RE       294                                                             */
/*  MI       330                                                             */
/*  FA       349                                                             */
/*  SOL      392                                                             */
/*  LA       440                                                             */
/*  SI       494                                                             */
/*                                                                           */
/*  Siguiente octava                                                         */
/*  DO       261*2                                                           */
/*  RE       294*2                                                           */
/*  .....                                                                    */
/*---------------------------------------------------------------------------*/
/* Se configura el prescaler para dividir entre 64. El valor a aplicar       */
/* al registro TMR0 se puede calcular usando la formula general o bien       */
/* mediante el script de octave/matlab:  timer0_TMR0_frec()                  */
/*  Ej. Para calcular el valor correspondiente a la nota DO:                 */
/*  > timer0_TMR0_frec(20,261)                                               */
/*  TMR0= 106, PS2:0= 101, Div=  64, Frec= 260.4 Hz, Error= 0.58 Hz          */
/*  TMR0= 181, PS2:0= 110, Div= 128, Frec= 260.4 Hz, Error= 0.58 Hz          */
/*  TMR0= 219, PS2:0= 111, Div= 256, Frec= 263.9 Hz, Error= -2.94 Hz         */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

// Valores de TMR0 correspondientes a las notas musicales
// Para prescaler a 64
#define DO  106
#define RE  123
#define MI  138
#define FA  144
#define SOL 156
#define LA  167
#define SI  177
#define DO_A 181

/************************************************************************/
/* Hacer una pausa generica, determinada por el valor inicial T0ini     */
/************************************************************************/
void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);
}


/********************************************************/
/* Generar la onda cuadrada correspondiente a la nota   */
/* ENTRADA:                                             */
/*   -valor: Valor de TMR0 para generar la nota         */
/********************************************************/
void nota(unsigned char n)
{
  unsigned char i;

  for (i=0; i<100; i++) {
    timer0_delay(n);
    LED=1;
    timer0_delay(n);
    LED=0;
  } 
}

/**************************/
/* Comienzo del programa  */
/**************************/
void main(void)
{

  //-- configurar pin RB1 para salida
  //-- Sera por donde salga la senal cuadrada
  TRISB1=0;
  
  //-- Configurar el timer0 en modo temporizador
  T0CS=0; PSA=0;

  //-- Configurar el prescaler (division entre 64)
  PS2=1; PS1=0; PS0=1;

  //-- Reproducir la escala musical
  nota(DO);
  nota(RE);
  nota(MI);
  nota(FA);
  nota(SOL);
  nota(LA);
  nota(SI);
  nota(DO_A);
  
  //-- Fin
  while(1);
}


