
//-- Especificar el pic a emplear
#include <pic16f876a.h>

#define LED RB1

__code unsigned char hex_digit[]={'0','1','2','3','4','5','6','7','8','9',
                           'A','B','C','D','E','F'};

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/***********************************************************************/
/* Leer un byte del SCI. Esta funcion se queda esperando hasta que     */
/* algun caracter llegue                                               */
/***********************************************************************/
unsigned char sci_read()
{
  //-- Esperar hasta que se reciba un dato
  while (RCIF==0);
  
  return RCREG;
}

/********************************************/
/* Enviar un byte por el SCI                */
/********************************************/
void sci_write(unsigned char dato)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (TXIF==0);
    
  //-- Hacer la transmision
  TXREG=dato;
}

void sci_cad(unsigned char *cad)
{
  unsigned char i=0;

  while (cad[i]!=0) {
    sci_write(cad[i]);
    i++;
  }
}

void sci_hexbyte(unsigned char car)
{
  unsigned char i;

  i = car >> 4;
  sci_write(hex_digit[i]);
  i = car&0x0F;
  sci_write(hex_digit[i]);
}

void sci_hexint(unsigned int dato)
{
  sci_hexbyte(dato>>8);
  sci_hexbyte(dato&0xFF);
}








