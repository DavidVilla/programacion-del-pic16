/*************************************************************************** */
/* eeprom-dump-sci.c                                                         */
/*---------------------------------------------------------------------------*/
/* MEMORIA EEPROM                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para volcar el contenido de la memoria eeprom por el puerto serie */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

unsigned char eeprom_read(unsigned char dir)
{
  EEADR=dir;  //-- Direccion a la que acceder
  EEPGD=0;
  RD=1;   //-- comenzar la lectura
  return EEDATA;
}

void main(void)
{
  unsigned char i;
  unsigned char valor;

  //-- Inicializar puerto serie
  sci_conf();

  //-- Configurar puerto B como salida
  TRISB=0;

  //-- Recorrer toda la memoria eeprom
  for (i=0; i<255; i++) {
  
    //-- Leer un byte
    valor=eeprom_read(i);

    //-- Sacarlo por los leds
    PORTB=valor;
  
    //-- Enviarlo por el sci
    sci_write(valor);
  }
  while(1);
}



