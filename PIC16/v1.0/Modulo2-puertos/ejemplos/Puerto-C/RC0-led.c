/*************************************************************************** */
/* RC0-led.c                                                                 */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO C                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de entrada digital por el puerto C. El pin RC0 se configura       */
/* como entrada y su valor se saca por el LED                                */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

void main(void)
{
  
  //-- Configurar el bit 0 como entrada
  TRISC0 = 1;

  //-- Configurar el led de la skypic para salida
  TRISB1=0;

  while(1) {
    //-- Sacar por el led el estado de RA0
    LED = RC0;
  }

}

