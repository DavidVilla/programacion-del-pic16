/*************************************************************************** */
/* icsp-virus1.c                                                             */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Este programa se auto-clona a si mismo. Es necesario conectar otra Skypic */
/* y ponerla en modo grabacion. Al dar al reset se empezara a clonar         */
/* La skypic con el software clonado puede a su vez, usarse para replicarse  */
/* en otras skypics                                                          */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valor inicial del TMR0 para realizar una pausa de 10ms
//-- Divisor del prescar a 256
#define T_10ms 61
#define T_1ms  236
#define T_2ms  217
#define T_3ms  197
#define T_4ms  178
#define T_100us 250

#define TIEMPO_100ms 10
#define TIEMPO_200ms 20
#define TIEMPO_20ms   2
#define TIEMPO_500ms 50
#define TIEMPO_1seg  100

#define LED RB1

//-- Pines por donde se sacan las senales de gracion
//-- En la Skypic destino las conexiones son:
//- CLOCK --> RB6
//- DATA ---> RB7
//- RESET --> MCLR

//-- En la Skypic grabadora las senales se sacan por estos pines:
#define CLOCK 1<<3    // Bit 3
#define DATA  1<<7    // Bit 7
#define RESET 1<<4    // Bit 4

//------------------------------------------------------------------------
//       CODIGOS DE LOS COMANDOS DEL PROTOCOLO ICSP DEL PIC 
//------------------------------------------------------------------------
#define READ_DATA   0x04  // Comando READ_DATA from Program Memory
#define LOAD_DATA   0x02  // Comando: LOAD DATA For Program Memory
#define BEG_ER_PRO  0x08  // Comando: BEGIN ERASE PROGRAMMING CICLE	
#define INC_ADDR    0x06  // comando: INCREMENT ADDRESS
#define LOAD_CONFIG 0x00  // Comando: LOAD CONFIGURATION
#define CHIP_ERASE  0x1F  // Comando: CHIP-ERASE


//-- Tamano del bloque a duplicar. El valor maximo es 0x2000 correspondiente
//-- a las 8K de la memoria Flash del PIC16F876A. El tiempo total de clonacion
//-- + verificacion es de 20 segundos
#define TAM 0x2000

//-- Lectura de palabras
unsigned char palabral;    //-- Parte baja
unsigned char palabrah;    //-- Parte alta

//-- Leer de la memoria flash
unsigned int flash_read(unsigned int dir)
{
  EEADRH = dir>>8;
  EEADR = dir&0xFF;
  EEPGD=1;
  RD=1;
  RD=1;
  RD=1;

  return EEDATH<<8 | EEDATA;
}

/******************************/
/* Hacer una pausa de 10ms    */
/******************************/
void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);

}

void delay(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}

/*****************************************/
/* Hacer un reset de la placa destino    */
/*****************************************/
void picp_reset() {
 
  //-- Hacer reset
  PORTB=RESET;
  
  //-- Esperar 20 ms
  delay(TIEMPO_20ms);
	
	//-- Desactivar reset
	PORTB=0;

  //-- Esperar 20 ms
  delay(TIEMPO_20ms);
}

/*------------------------------------------------------------------*/
/*  FUNCIONES DE IMPLEMENTACION DEL PROTOCOLO ICSP                  */
/*------------------------------------------------------------------*/

/*********************************************/
/* Enviar bits por DATA                      */
/* Se comienza por los menos significativos  */
/* ENTRADAS:                                 */
/*     -dato: Byte a enviar                  */
/*     -nbits: numero de bits a enviar       */
/*********************************************/
void write_bits(unsigned char dato, unsigned char nbits)
{
  unsigned char i;
  
  
  for (i=0; i<nbits; i++) {
    
    //-- Enviar bit menos significativo de dato
    if ((dato & 0x01)==0)
      PORTB = PORTB & ~(DATA);  //-- Enviar un cero
    else 
      PORTB = PORTB | DATA;     //-- Enviar un uno
    
    //-- Activar el reloj
    PORTB = PORTB | CLOCK;  //-- Flanco de subida
    PORTB = PORTB & ~CLOCK; //-- Flanco de bajada
    
    //-- Acceder al siguiente bit de dato
    dato = dato >> 1;
  }  
}

/**********************************************************************/
/* Leer a traves del ICSP                                             */
/* ENTRADA: Numero de bits a leer                                     */
/* DEVUELVE: los bits leidos. Se comienza a insertar los bits por el  */
/*    bit mas significativo                                           */
/**********************************************************************/
unsigned char read_bits(unsigned char nbits)
{
  unsigned char dato=0x00;
  unsigned char i;
  
  for (i=0; i<nbits; i++) {
    dato=dato>>1;
  
    //-- Flanco de reloj: subida y baja.
    //-- A continuacion el dato esta listo
    PORTB = PORTB | CLOCK;
    PORTB = PORTB & ~CLOCK;
  
    if ((PORTB & DATA)==0X00)
      dato=dato & ~0x80; //-- Recibido un 0
    else
      dato=dato | 0x80;  //-- Recibido un 1
  
  }
  
  return dato;
}

/**********************************/
/* Enviar un comando por el ICSP  */
/* ENTRADAS:                      */
/*   -cmd : Comando a enviar      */
/**********************************/
void send_cmd(unsigned char cmd)
{
  //-- Poner el reloj a cero
  PORTB = PORTB & ~CLOCK;
  write_bits(cmd,0x6);
  
  //-- Poner DATA a '1' para indicar fin del comando
  PORTB = PORTB | DATA;
  
  //-- Aqui debe haber al menos una espera de 1us
  //-- A una frecuencia de 20Mhz, el pic tarda en ejecutar cada
  //-- instruccion 200ns. Al menos se tienen que ejecutar 5
  //-- Ponemos 6 instrucciones nop
  _asm 
    nop
    nop
    nop
    nop
    nop
    nop
  _endasm;
}

/***************************************/
/* Enviar un dato por el ICSP          */
/* Son palabras de 14 bits             */
/***************************************/
void send_data2(unsigned int dato)
{
  unsigned char datol;

  datol=dato&0xFF;

  //-- Enviar bit de start (0)
  write_bits(0x00,1);

  //-- Enviar byte bajo
  write_bits(datol,8);

  //-- Enviar byte alto
  write_bits(dato>>8,7);

  //-- Pausa para enviar el siguiente comando
  timer0_delay(T_100us);

}

/*************************************************************/
/* Recibir un dato de 14 bits por el ICSP                    */
/* Se actualizan las variables globales palabral y palabrah  */
/*************************************************************/
unsigned int recv_data()
{
  //unsigned char i;
  
  //-- Configurar pin DATA para entrada
  TRISB = TRISB | DATA;
 
  //-- Leer bit de start y descartarlo 
  read_bits(1);
  
  //-- Leer 8 bits menos significativos
  palabral=read_bits(8);
  
  //-- Leer los 6 bits mas significativos + el bit de stop
  palabrah=read_bits(7);
  
  //-- Alinear la parte alta para que los bits menos significativos
  //-- esten a la derecha. Los dos mas significativos se ponen a 0
  palabrah=(palabrah>>1)&0x3F; 

  //-- Configurar pin DATA para salida
  TRISB = TRISB & ~DATA;

  //-- Pausa antes de realizar la siguiente lectura
  timer0_delay(T_100us);

  return palabrah<<8 | palabral;
}

/*********************************************************************/
/* Error 1: Pic destino no detectado. El led parpadea, pero estando  */
/* mas tiempo apagado que encendido                                  */
/*********************************************************************/
void error1()
{
  while (1) {
    LED=1;
    delay(TIEMPO_100ms);
    LED=0;
    delay(TIEMPO_1seg);
  }
}

/***************************************************************************/
/* Error: Error en la verificacion. Lo grabado es diferente de la flash    */
/* de la Skypic padre                                                      */
/* El led parpadea, pero esta mas tiempo encendido que apagado             */
/***************************************************************************/
void error2()
{
  while (1) {
    LED=1;
    delay(TIEMPO_1seg);
    LED=0;
    delay(TIEMPO_100ms);
  }
}

/***********************/
/* Programa principal  */
/***********************/
void main(void)
{
  unsigned int id;
  unsigned int j;
  unsigned int i;
  unsigned int dato;
  unsigned int dato_flash, dato_copia;

  //-- Configurar el puerto B
  //-- CLOCK y RESET son de salida
  //-- DATA es entrada/salida pero inicialemnte se pone como salida
  TRISB = ~(CLOCK | DATA | RESET| 0x02);

  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 256
  PS2=1; PS1=1; PS0=1;

  //-- Esperar un tiempo antes de comenzar
  delay(TIEMPO_20ms);

  //-------------------------------------------------
  //-- FASE 1: Identificacion del PIC destino 
  //------------------------------------------------
  //-- Hacer reset del pic destino
  picp_reset();

  //-- Acceso a la memoria de configuracion
  send_cmd(LOAD_CONFIG); 
  send_data2(0x00);

  //-- Avanzar 6 posiciones hasta la direccion 2006
  for (j=0; j<6; j++) 
    send_cmd(INC_ADDR);

  //-- Enviar comando READ_DATA
  send_cmd(READ_DATA); 
  
  //-- Leer la identificacion
  id=recv_data();

  //-- Error 1: PIC destino no detectado
  if (id==0x3FFF) error1();

  //-------------------------------------------------
  //-- FASE 2: DUPLICACION! 
  //-------------------------------------------------
  //-- Reset de la placa destino
  picp_reset();

  LED=1;

  //-- Comenzar duplicacion!! Recorrer la memoria flash. Leer las palabras
  //-- y grabarlas en el PIC destino
  for (i=0; i<TAM; i++) {
    
    //-- Leer direccion i de la memoria flash del pic local
    dato = flash_read(i);
    
    //-- Enviar el comando de Load data
    send_cmd(LOAD_DATA);

    //-- Enviar la palabra
    send_data2(dato);

    //-- Cada 8 bytes cargados se ejecuta el comando de grabacion
    if (i%8==7) {
      send_cmd(BEG_ER_PRO);
      timer0_delay(T_10ms);
    } 

    //-- Incrementar puntero del pic destino
    send_cmd(INC_ADDR);

    //-- Mostrar actividad en el led
    PORTB=PORTB^0x02;

  }

  //-------------------------------------------------
  //-- FASE 3: VERIFICACION 
  //-------------------------------------------------
  //-- Comenzar verificacion
  picp_reset();
    
  //-- Recorrer la memoria comparando lo que hay en la flash con lo guardado
  //-- en el pic destino
  for (i=0; i<TAM; i++) {

    //-- Leer paralabra de la flash
    dato_flash = flash_read(i);

    //-- Enviar comando READ_DATA
    send_cmd(READ_DATA); 
  
    //-- Leer la palabra del destino
    dato_copia=recv_data();

    //-- Comparar. Si son iguales todo OK. Si no es que hay error
    if (dato_flash!=dato_copia) error2();

    //-- Incrementar puntero
    send_cmd(INC_ADDR);

    //-- Mostrar actividad en el led
    PORTB=PORTB^0x02;
  }

  //-------------------------------------------------
  //-- FASE 4: Grabar Palabra de configuracion 
  //-------------------------------------------------
  //-- Se graba la palabra 0x3F3A

  //-- Reset del pic destino
  picp_reset();

  //-- Acceso a la memoria de configuracion
  send_cmd(LOAD_CONFIG); 
  send_data2(0x00);

  //-- Avanzar 7 posiciones hasta la direccion 2007
  for (j=0; j<7; j++) 
    send_cmd(INC_ADDR);

  //-- Enviar el comando de Load data
  send_cmd(LOAD_DATA);

  //-- Enviar la palabra
  send_data2(0x3F3A);

  //-- Grabarla!
  send_cmd(BEG_ER_PRO);

  //---------------------------------------------
  //-- Duplicacion finalizada con exito!!!!!
  //---------------------------------------------
  //-- Encender el led para indicar que todo OK!
  LED=1;

  while(1);
}


