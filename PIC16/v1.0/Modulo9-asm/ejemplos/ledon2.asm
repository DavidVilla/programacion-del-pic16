;****************************************************************************
;*  ledon2.asm                                                              *
;*--------------------------------------------------------------------------*
; Ejemplo para la Skypic                                                    *
;---------------------------------------------------------------------------*
; Ejemplo "Hola mundo" para encender el led conecado a RB1                  *
; Contiene el codigo de arranque necesario para cargarlo con el Bootloader  *
;---------------------------------------------------------------------------*
;  LICENCIA GPL                                                             *
;****************************************************************************
 
; --- Especificar el PIC a emplear
  INCLUDE "p16f876a.inc"

; -- Comienzo del programa
  ORG 0

  ;-- Este codigo es para iniciar el Bootloader. Si no hay bootloader,
  ;-- el codigo no hace nada. Simplemente salta al comienzo del 
  ;-- codigo. Pero si hay bootloader, saltara a ejecutarlo
  CLRF    STATUS
  MOVLW   0        ;--> Esta instruccion la modifica el bootloader en la carga
  MOVWF   PCLATH
  GOTO    start

;-- Programa principal
start
; -- Poner el bit RB1 como salida
  BSF STATUS,RP0    ; Acceder al banco 1
  BCF TRISB,1       ; Poner RB1 como salida

; -- Activar RB1 para encender el led
  BCF STATUS,RP0    ; Acceder al banco 0
  BSF PORTB,1       ; Sacar un '1' por RB1

fin GOTO fin     ; Bucle infinito

  END
  
