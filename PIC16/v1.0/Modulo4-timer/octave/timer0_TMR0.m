#--------------------------------------------------------------------------------
#-- Calcular el valor del registro TMR0, del prescaler, el tiempo real y el error
#---------------------------------------------------------------------------------
#-- ENTRADAS:
#--  -FOSC: Frecuencia del cristal en Mhz
#--  -t: Tiempo de espera (en micro-segundos) 
#----------------------------------------------------------------------------------
#-- Ejemplo: Realizar pausas de 10ms (10000 micro-segundos) con un Pic de 20Mhz
#--
#-- octave> timer0_TMR0(20,10000)
#-- T0INI=  61, PS2:0= 111, Div= 256, tc= 9984.0 us, Error= 16.00 us
#
#-- Donde, T0INI -> Valor a asignar al registro TMR0
#-- PS2:0 --> Bits de configuración del prescaler
#-- Div  ---> Valor del dividor
#-- tc ----- Tiempo real que se espera
#-- error --> Error (diferencia entre tiempo real y el tiempo especificado
#-----------------------------------------------------------------------------------


function TMR0=timer0_TMR0(FOSC,t)

#-- Valores posibles de los divisores
DIV=[2,4,8,16,32,64,128,256];

#-- Valores del prescaler asociados a cada uno de los divisores
PS=[000,001,010,011,100,101,110,111];

#-- Calcular el valor en ticks para la inicializacion del timer 0
TMR0=256-timer0_ticks(FOSC,t);

#-- Calcular el tiempo real en micro-segundos que medira el timer 0
tc=timer0_Duracion(FOSC,t);

#-- Calcular el error (tiempo especificado - tiempo real)
Error = t - tc;

#-- Mostrar los datos al usuario
for i=1:length(TMR0)
  if TMR0(i)<256 && TMR0(i)>=0
    printf ("T0INI= %3d, PS2:0= %03d, Div= %3d, tc= %.1f us, Error= %.2f us\n",
             TMR0(i),PS(i), DIV(i), tc(i),Error(i));
  endif;           
endfor


return
