/*************************************************************************** */
/* salida6.c                                                                 */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO A                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de salida de 6 bits                                               */
/* Se ponen todos los bits del puerto A a 1                                  */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void main(void)
{
  //-- Configurar todos los bits del puerto B como salida
  TRISA=0x00;

  //-- Configurar puerto A como puerto Digital
  ADCON1=0x06;

  //-- Poner todos los pines a 1
  PORTA = 0xFF;

  //-- Bucle infinito
  while(1);
}

