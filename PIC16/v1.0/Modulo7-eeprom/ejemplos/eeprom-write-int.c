/*************************************************************************** */
/* eeprom-write-int.c                                                        */
/*---------------------------------------------------------------------------*/
/* MEMORIA EEPROM                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de escritura de una tira de bytes en la eeprom                    */
/* La escritura se hace por interrupciones                                   */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

unsigned char tabla[]={'P','O','D','E','M','O','S','!','!'};
unsigned char size = sizeof(tabla)/sizeof(unsigned char);
unsigned char i=0;

void isr() interrupt 0
{
  //-- Quitar flag de interrupcion
  EEIF=0;

  //-- Apuntar al siguiente caracter de la tabla
  i++;

  //-- Si es el ultimo caracter terminar
  if (i==size) {
    RB1=1;
    return;  //-- terminar
  }

  EEADR=i;           //-- Establecer direccion
  EEDATA=tabla[i];   //-- Establecer el dato
  EEPGD=0;           //-- Indicar acceso a la memoria eeprom

  WREN=1;
  EECON2=0x55;
  EECON2=0xAA;
  WR=1;           //-- Comenzar la escritura!
  
}

void main(void)
{
  
  //-- Configurar led
  TRISB1=0;

  //-- Quitar flag de interrupcion
  EEIF=0;

  //-- Activar las interrupciones de escritura en la eeprom
  EEIE=1;
  PEIE=1;
  GIE=1;

  //-- Escribir el primer caracter en la direccion 0
  EEADR=0;  
  EEDATA=tabla[0];   
  EEPGD=0; 

  WREN=1;
  EECON2=0x55;
  EECON2=0xAA;
  WR=1;  

  //-- El resto de caracteres se escriben mediante interrupciones

  while(1);
}
