/*************************************************************************** */
/* ad0-leds.c                                                                */ 
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* CONVERSOR ANALOGICO-DIGITAL                                               */
/*---------------------------------------------------------------------------*/
/* Ejemplo de lectura del cana RA0 analogico. La muestra de 8 bits leida     */
/* se envia por el puerto B para ser visualizada en los leds                 */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void main(void)
{
  //-- Configurar puerto B para salida
  TRISB = 0x00;

  //-- Configurar RA0 como entrada analogica. El resto del puerto A
  //-- como digital
  ADCON1 = 0x0E;

  //-- Configurar pin RA0 como entrada
  TRISA0 = 1;

  //-- Leer los 8 bits mas significativos por ADRESH
  ADFM = 0;  
  
  //-- Seleccionar el canal analogico (RA0)
  ADCON0 = 0x00;

  //-- Encender conversor
  ADON=1;

  while(1) {
    //-- Poner flag a 0
    ADIF=0;

    //-- Que comience la conversion!!!
    GO=1;

    //-- Esperar a que finalice la conversion
    while(ADIF==0);

    //-- Enviar los 8 bits mas significativos a los leds
    PORTB=ADRESH;
  }
  
}



