/*************************************************************************** */
/* variables1.c                                                              */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de declaracion de variables y su uso. Se realizan operaciones     */
/* sencillas con las variables y el resultado se muestra por el puerto B     */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define PUERTOB_SALIDA TRISB=0x00

void main(void)
{
  //-- Declaracion de dos variables de 8 bits, sin signo
  //-- Rango: 0 - 255
  unsigned char i;
  unsigned char j;
  
  //-- Configurar puerto B como salida
  PUERTOB_SALIDA;

  //-- Asignacion de valores a las variables
  i=1;
  j=i+2;

  //-- Sacar el resultado por el puerto B para visualizarlo en la Freeleds
  PORTB = j;
}

