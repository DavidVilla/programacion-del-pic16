/*************************************************************************** */
/* servo8.c                                                                  */
/*---------------------------------------------------------------------------*/
/* CONTROL DE SERVOS                                                         */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para posicionar hasta 8 servos, mediante espera activa            */
/* Se mueven los servos 0 y 1. El resto se dejan en su posicion central      */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Prescaler a 64
//-- Ventana temporal para el servo en ticks (2.5ms)
#define T           195
#define T0INI_2_5ms 256-195

//-- Posiciones de los 8 servos
int pos[]={0,0,0,0,0,0,0,0};

//-- Mascara con la activacion de cada uno de los servos
unsigned char mask[]={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/**************************************************************************/
/* Generar una senal PWM para posicionar el servo con el angulo indicado  */
/* El angulo debe estar en el rango (-90,90)                              */
/**************************************************************************/
void servo_pos() 
{
  unsigned int ciclos;
  unsigned int i;
  char Ton;
  
  for (ciclos=0; ciclos<100; ciclos++) {

    //-- Cada iteracion es una ventana de 2.5ms
    for (i=0; i<8; i++) {
      //-- Calcular el ancho del pulso en funcion de los grados
      //-- La posicion central se corresponde con 1.3ms. Un extremo con 2.3 y 
      //-- el otro con 0.3
      Ton = (78*pos[i])/90 + 102;

      //-- Poner el pin correspondiente a 1
      PORTB=mask[i];
      timer0_delay(255-Ton);

      //-- Poner los pines a 0
      PORTB=0x00;
      timer0_delay(255-T+Ton);

    }
  }
}


void main(void)
{
  //-- Puerto B de salida
  TRISB=0;

  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler A 64
  PS2=1; PS1=0; PS0=1;

  while(1) {
    //-- Posicionar servos 1 y 2 en los extremos
    pos[0]=90; pos[1]=-90;
    servo_pos();

    //-- Posicionar servos 1 y 2 en los extremos opuestos
    pos[0]=-90; pos[1]=90;
    servo_pos(); 
  } 

}
