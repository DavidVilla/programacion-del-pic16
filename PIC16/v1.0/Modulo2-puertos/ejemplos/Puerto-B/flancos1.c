/*************************************************************************** */
/*  flancos1.c                                                               */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de deteccion de cambio en el pin RB7. Cada vez que ocurra un      */
/* flanco de subida o bajada por RB7, se cambiara el estado del led          */
/* Para la deteccion se usa el flag RBIF que se activa cada vez que hay      */
/* un cambio en alguno de los pines RB7, RB6, RB5 y RB4                      */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

volatile unsigned char temp;

void main(void)
{
  //-- Configurar RB7 para entrada y resto salidas
  TRISB=0x80;

  //-- Activar pull-ups del puerto B
  NOT_RBPU=0;

  while(1) {

    //-- Para quitar flag hay que leer primero del puerto
    temp=PORTB;

    //-- Quitar flag de cambio detectado
    RBIF=0;

    //-- Esperar a que ocurra algún cambio
    while(RBIF==0);

    //-- Cambiar el estado del led
    LED^=1;

  }

}

