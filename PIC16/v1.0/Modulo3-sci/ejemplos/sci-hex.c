/*************************************************************************** */
/* sci-hex.c                                                                 */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Conversion de numeros a cadenas ASCII                                    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

//-- Especificar el pic a emplear
#include <pic16f876a.h>

#define LED RB1

__code unsigned char hex_digit[]={'0','1','2','3','4','5','6','7','8','9',
                           'A','B','C','D','E','F'};

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/***********************************************************************/
/* Leer un byte del SCI. Esta funcion se queda esperando hasta que     */
/* algun caracter llegue                                               */
/***********************************************************************/
unsigned char sci_read()
{
  //-- Esperar hasta que se reciba un dato
  while (RCIF==0);
  
  return RCREG;
}

/********************************************/
/* Enviar un byte por el SCI                */
/********************************************/
void sci_write(unsigned char dato)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (TXIF==0);
    
  //-- Hacer la transmision
  TXREG=dato;
}

void sci_cad(unsigned char *cad)
{
  unsigned char i=0;

  while (cad[i]!=0) {
    sci_write(cad[i]);
    i++;
  }
}

void sci_hexbyte(unsigned char car)
{
  unsigned char i;

  i = car >> 4;
  sci_write(hex_digit[i]);
  i = car&0x0F;
  sci_write(hex_digit[i]);
}

void sci_hexint(unsigned int dato)
{
  sci_hexbyte(dato>>8);
  sci_hexbyte(dato&0xFF);
}

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned int test[]={0x1234, 0xAABB, 0x0000, 0xFFFF};
  unsigned int size = sizeof(test)/sizeof(int);
  
  unsigned char n;
  unsigned int j;
  

  //-- Puerto B de salida
  TRISB=0;

  //-- Configurar las comunicaciones serie
  sci_conf();

  sci_cad("\nProbando hexint\n");
  for (j=0; j<size; j++) {
    sci_hexint(test[j]);
    sci_write(' ');
  }

  sci_cad("\n\nProbando hexbyte");
  for (n=0; n<255; n++) {
    if (n%16==0) sci_write('\n');
    sci_hexbyte(n);
    sci_write(' ');
  }
 
  while(1);
}







