/*************************************************************************** */
/* luces.c                                                                   */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de utilizacion de tablas en C para la generacion de secuencias    */
/* en los leds conectados al puerto B                                        */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Tabla con los valores a sacar por los leds
unsigned char tabla[]={0x55,0xAA};

/**********************************************/
/* Funcion de pausa                           */
/* ENTRADA: unidades de tiempo para la pausa  */
/**********************************************/
void pausa(unsigned char tiempo)
{
  unsigned char i;
  unsigned int j;
  int temp=0;

  //-- Se realizan dos bucles anidados
  for (i=0; i<tiempo; i++) {
    for (j=0; j<0x8000; j++) {
      //-- Operacion inutil, para consumir tiempo
      temp=temp+1;
    }
  }
}

void main(void)
{
  unsigned char i;

  //-- Configurar puerto B para salida
  TRISB=0x00;

  while(1) {

    //-- Recorrer todos los elementos de la tabla
    for (i=0; i<2; i++) {

      //-- Sacar el valor i por los leds
      PORTB=tabla[i];

      //-- Pausa
      pausa(2);
    }
  }

}

