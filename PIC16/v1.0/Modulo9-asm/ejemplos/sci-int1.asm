;****************************************************************************
;*  sci-int1.asm                                                            *
;---------------------------------------------------------------------------*
; Ejemplo para la tarjeta SKYPIC                                            *
;---------------------------------------------------------------------------*
; Prueba de la interrupcion de recepcion del SCI                            *
; Se hace eco de todo lo recibido, mediante interupciones. El programa      *
; principal no hace nada                                                    *
;---------------------------------------------------------------------------*
;  LICENCIA GPL                                                             *
;****************************************************************************

;-- Establecer el PIC a emplear
  INCLUDE "p16f876a.inc"

  ORG 0
  ;-- Codigo de inicializacion para bootloader
  CLRF    STATUS
  MOVLW   0
  MOVWF   PCLATH
  GOTO    start

;--- Punto de entrada de interrupciones
  ORG 0x0004    ; Vector de Interrupci�n

  ;;-- Cambiar el led de estado
  MOVLW 0x02
  XORWF PORTB,F
  
  ;;-- Leer caracter para quitar flag de interrupcion
  MOVFW RCREG 

  RETFIE

;--- Comienzo del programa
start

;-- Configuracion del puerto serie
  BSF STATUS,RP0    ; Acceso al banco 1
  MOVLW 0x81        ; Velocidad: 9600 baudios
  MOVWF SPBRG
  MOVLW 0x24
  MOVWF TXSTA       ; Configurar transmisor
  BCF STATUS,RP0    ; Acceso al banco 0		
  MOVLW 0x90        ; Configurar receptor
  MOVWF RCSTA

;-- Configurar Bit RB1 para salida
  BSF STATUS,RP0    ; Acceso al Banco 1
  BCF TRISB,1       ; RB1 como salida

;-- Configurar las interrupciones
  BSF PIE1,RCIE     ; Activar interrupcion del receptor SCI
  BSF INTCON,PEIE   ; Activar interrupci�n de perif�ricos
  BSF INTCON,GIE    ; Activar interrupciones globales

  BCF STATUS,RP0    ; Acceso al banco 0
  BSF PORTB,1       ; Encender led

;------------------------
;-    BUCLE PRINCIPAL
;------------------------
main
  GOTO main

  END
