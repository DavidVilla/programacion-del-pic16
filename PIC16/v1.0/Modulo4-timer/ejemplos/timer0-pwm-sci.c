/*************************************************************************** */
/* timer0-pwm-sci.c                                                          */  
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de Pwm mediante interrupciones. Los ciclos se trabajo se pueden  */
/* establecer mediante el puerto serie. Se puede establecer desde el 10%     */
/* hasta el 90%.                                                             */
/* Este ejemplo permite controlar la velocidad de los motores del robot      */
/* Skybot                                                                    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/


#include <pic16f876a.h>
#include "sci.h"

// Bits para el manejo de los motores del Skybot
#define M2_ON  RB4
#define M2_DIR RB3
#define M1_ON  RB2
#define M1_DIR RB1

//-- Estados del automata de generacion del PWM
#define ESTADO_ON  0
#define ESTADO_OFF 1

//-- Ciclos de trabajo
unsigned char ct[]={26,51,77,103,128,154,179,205,230};

unsigned char estado=0;
unsigned char T0ini;

//-- Interfaz entre la rutina de atención a la int.
unsigned char Ton=0;             //-- Senal PWM

/*****************************************/
/* Rutina de atencion a la interrupcion  */
/*****************************************/
void isr() interrupt 0 
{
  //-- En esta estado se activa el pulso
  if (estado==ESTADO_ON) {

    //-- Activar los motores
    M1_ON=1; M2_ON=1;

    //-- Valor para que esté activo durante Ton ticks
    T0ini=255-Ton;

    //-- Pasar al estado siguiente
    estado=ESTADO_OFF;
  }
  //---- En este estado se desactivan los pulsos
  else {

    //-- Desactivar los motores
    M1_ON=0; M2_ON=0;

    //-- Valor para que esté activo durante Toff ticks
    T0ini=Ton;

    //-- Pasar al estado inicial
    estado=ESTADO_ON;
  }

  //-- Dar valor inicial del timer
  TMR0=T0ini;

  //-- Quitar flag de interrupcion
  TMR0IF=0;
}

void main(void)
{
  unsigned char c;
  unsigned char i;

  TRISB=0xE1;

  M2_DIR=1; M1_DIR=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 32
  PS2=1; PS1=0; PS0=0;

  //-- configurar puerto serie
  sci_conf();

  //-- Activar interrupciones globales,
  //-- pero todavia no las del timer0
  GIE=1;

  //-- El bucle principal se encarga de leer las teclas recibidas
  //-- y establecer el pwm solicitado
  while(1) {
    
    //-- Leer la tecla
    c=sci_read();

    //-- Teclas de control del pwm. Establecer el ciclo de trabajo
    if (c>='1' && c<'9') {
      i=c-'1';
      Ton=ct[i];
      M1_DIR=0; M2_DIR=1;
      TMR0IE=1; 
    }

    //-- Otras teclas para mover el robot. Sin pwm.
    switch(c) {
      case 'q': 
        TMR0IE=0;
        M1_ON=1;  M2_ON=1;
        M1_DIR=0; M2_DIR=1;
        break;
      case 'a':
        TMR0IE=0;
        M1_ON=1;  M2_ON=1;
        M1_DIR=1; M2_DIR=0;
        break;
      case 'o': 
        TMR0IE=0;
        M1_ON=1;  M2_ON=1;
        M1_DIR=1; M2_DIR=1;
        break;
      case 'p':
        TMR0IE=0;
        M1_ON=1;  M2_ON=1;
        M1_DIR=0; M2_DIR=0;
        break;
      case ' ':
        TMR0IE=0; PORTB=0;
        break;
    }
  }
}

