/*************************************************************************** */
/* sci-int-rx.c                                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de la interrupcion de recepcion. Cada vez que se recibe un        */
/* caracter se lee mediante interrupciones y se cambia el estado del led     */
/* El bucle principal no hace nada                                           */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

volatile unsigned char c;

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/*****************************************/
/* Rutina de atencion a la interrupcion  */
/*****************************************/
void isr() interrupt 0 
{
  //-- Quitar flag de interrupcion (hay que leer el dato)
  c=RCREG;

  //-- Cambiar led de estado                                                                                          /* rutina de servicio de interrupciones */
  LED^=1;
}


/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  //-- Configurar puerto B para salida
  TRISB=0;

  //-- Configurar puerto serie
  sci_conf();

  //-- Activar las interrupciones
  RCIE=1;
  PEIE=1;
  GIE=1;
  
	while(1);

}

