/*************************************************************************** */
/* timer0-onda10Khz-int.c                                                    */  
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de generacion de una onda cuadrada de 10Khz mediante             */
/* Interrupciones. Si se conecta un altavoz a cualquiera de los pines del    */
/* puerto B se escuchara un tono                                             */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valor inicial del TMR0 para realizar una pausa 
//-- corresponiente al semiperiodo de una onda de 10Khz
//-- Divisor del prescar a 2
#define T_10Khz 131

unsigned char cont=0;

void isr() interrupt 0 
{
  //-- Incrementar contador
  cont++;

  //-- y enviarlo por el puerto B. El bit menos significativo tendra la 
  //-- maxima frecuencia de 10Khz. Los siguientes, la mitad, la cuarta parte
  //-- etc... ya que el contador hace de prescaler
  PORTB=cont;

  //-- Dar valor inicial del timer
  TMR0=T_10Khz;

  //-- Quitar flag de interrupcion
  TMR0IF=0;
}


void main(void)
{
  TRISB=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 2
  PS2=0; PS1=0; PS0=0;

  //-- Activar interrupciones
  TMR0IE=1;
  GIE=1;

  //-- El bucle principal no hace nada
  while(1);
}

