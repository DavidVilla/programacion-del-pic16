/*************************************************************************** */
/* RA0-led.c                                                                 */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO A                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de configuracion del puerto A como digital. El bit RA0 se         */
/* configura como entrada y su estado se saca por el led de la Skypic        */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

void main(void)
{
  //-- Todos los pines como E/S Digitales
  ADCON1=0x06;

  //-- Configurar el bit 0 como entrada
  TRISA0 = 1;

  //-- Configurar el led de la skypic para salida
  TRISB1=0;

  while(1) {
    //-- Sacar por el led el estado de RA0
    LED = RA0;
  }

}

