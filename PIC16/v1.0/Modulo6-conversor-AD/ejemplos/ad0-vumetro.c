/*************************************************************************** */
/* ad0-vumetro.c                                                             */ 
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* CONVERSOR ANALOGICO-DIGITAL                                               */
/*---------------------------------------------------------------------------*/
/* Ejemplo de lectura del cana RA0 analogico. La lectura se realiza          */
/* mediante interrupciones. La informacion se muestra en los leds,           */
/* simulando un vumetro.                                                     */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valores a sacar por los leds
unsigned char vumetro[]={0x00,0x01,0x03,0x07,0x0F,0x1F,0x3F,0x7F,0xFF};
unsigned char indice;
unsigned char muestra;

void isr() interrupt 0 
{
  //-- Quitar flag de interrupcion
  ADIF=0;

  //-- Leer la muestra
  muestra=ADRESH;

  //-- Convertir la muestra a valores del vumetro
  if (muestra==0xFF) indice=8;
    //-- Usar los 3 bits mas significativos como indice para la tabla
  else indice=muestra>>5;

  //-- Sacar el valor por los leds
  PORTB=vumetro[indice];

  //-- Comenzar otra conversion
  GO=1;
}

void main(void)
{
  //-- Configurar puerto B para salida
  TRISB = 0x00;

  //-- Configurar RA0 como entrada analogica. El resto del puerto A
  //-- como digital
  ADCON1 = 0x0E;

  //-- Configurar pin RA0 como entrada
  TRISA0 = 1;

  //-- Leer los 8 bits mas significativos por ADRESH
  ADFM = 0;  
  
  //-- Seleccionar el canal analogico (RA0)
  ADCON0 = 0x00;

  //-- Encender conversor
  ADON=1;

  //-- Activar las interrupciones
  ADIE=1;
  PEIE=1;
  GIE=1;

  //-- Poner flag del converso a 0
  //-- Poner flag a 0
  ADIF=0;

  //-- Que comience la conversion!!!
  GO=1;

  //-- El bucle principal no hace nada
  while(1);
  
}



