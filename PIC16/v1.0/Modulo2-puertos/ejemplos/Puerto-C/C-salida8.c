/*************************************************************************** */
/* C-salida8.c                                                               */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO C                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de salida de 8 bits                                               */
/* Enviar un valor de 8 bits por el puerto C                                 */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void main(void)
{
  //-- Configurar todos los bits del puerto B como salida
  TRISC=0x00;

  //-- Enviar un byte a los leds
  PORTC = 0x55;

  //-- Bucle infinito
  while(1);
}

