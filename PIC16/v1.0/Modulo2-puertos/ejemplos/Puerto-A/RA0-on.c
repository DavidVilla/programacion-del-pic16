/*************************************************************************** */
/* RA0-on.c                                                                  */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO A                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de configuracion del puerto A como digital. El bit RA0 se         */
/* usa como salida. Este ejemplo simplemente lo pone a 1 para visualizarlo   */
/* en los leds                                                               */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void main(void)
{
  //-- Todos los pines como E/S Digitales
  ADCON1=0x06;

  //-- Configurar el bit 0 como salida
  TRISA0 = 0;

  //-- Activar bit 0
  RA0 = 1;

  //-- Bucle infinito
  while(1);
}

