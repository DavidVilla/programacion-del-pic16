/*************************************************************************** */
/* contador1.c                                                               */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Contador desde 0 hasta 128 que se muestra por los leds conectados al      */
/* puerto B... Pero se hace tan rápido que sólo veremos el valor final       */
/* Con este ejemplo nos haremos una idea de la velocidad con la que          */
/* ejecuta instrucciones el PIC                                              */ 
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define PUERTOB_SALIDA TRISB=0x00

void main(void)
{
  unsigned char i;
  
  //-- Configurar puerto B como salida
  PUERTOB_SALIDA;

  //-- Bucle desde i=0 hasta 100. La variable i toma los valores
  //-- 0, 1, 2, 3.....
  for (i=0; i<=128; i++) {
    PORTB=i;  //-- Visualizar la variable i en los leds
  }

  //-- Terminar
  while(1);

}

