/*************************************************************************** */
/* contador-int2.c                                                           */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de uso de la entrada RB0. Cada vez que se reciba un flanco por    */
/* de bajada por RB0 se cambiara el estado del led                           */
/*   Ahora se realiza mediante INTERRUPCIONES. En vez de esperar a que se    */
/* active el flag INTF, cuando se recibe el flanco de bajada automaticamente */
/* se llama a la rutina de atencion a la interrupcion                        */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void isr() interrupt 0 
{                                                                                                   /* rutina de servicio de interrupciones */
  //-- Poner flag de interrupcion a 0
  INTF=0; 

  //-- Cambiar el LED de estado
  RB1^=1;
}

void main(void)
{
  //-- Configuracion puerto B: Todos los bits de salida
  //-- excepto RB0 que es de entrada
  TRISB=0x01;

  //-- Configurar interrupcion por flanco de bajada
  INTEDG=0;

  //-- Habilitar interrupcion por RB0
  INTE=1;

  //-- Poner a cero flag de interrupcion
  INTF=0;

  //-- Habilitar las interrupciones en general
  GIE=1;

  //-- Bucle principal
  while(1); 

}

