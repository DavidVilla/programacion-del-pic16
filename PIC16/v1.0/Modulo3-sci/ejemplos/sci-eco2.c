/*************************************************************************** */
/* sci-eco2.c                                                                */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de envio-recepcion de datos. Se hace eco de todo lo recibido por */
/* el puerto serie. Ademas se saca tambien por el puerto B para depurar      */
/* Se han creado funciones especificas para la lectura de datos y el envio   */
/* por el puerto serie. Esto nos permitira reutilizarlas en los demas        */
/* ejemplos                                                                  */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/***********************************************************************/
/* Leer un byte del SCI. Esta funcion se queda esperando hasta que     */
/* algun caracter llegue                                               */
/***********************************************************************/
unsigned char sci_read()
{
  //-- Esperar hasta que se reciba un dato
  while (RCIF==0);

  //-- Leer dato (para que Flag se ponga a 0)
  return RCREG;
}

/********************************************/
/* Enviar un byte por el SCI                */
/********************************************/
void sci_write(unsigned char dato)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (TXIF==0);
    
  //-- Hacer la transmision
  TXREG=dato;
}

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned char c;
  
  //-- Configurar el puerto B para salida
  TRISB=0;

  //-- Configurar puerto serie
  sci_conf();
  
  while(1) {

    //-- Leer dato (se queda esperando hasta que llega)
    c=sci_read();

    //-- Sacar dato recibido por los leds
    PORTB=c;

    //-- Hacer eco del carácter recibido. Enviarlo de vuelta al PC
    sci_write(c);
    
  }

}

