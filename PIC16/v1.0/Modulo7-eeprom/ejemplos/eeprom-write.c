/*************************************************************************** */
/* eeprom-write.c                                                            */
/*---------------------------------------------------------------------------*/
/* MEMORIA EEPROM                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de escritura de una tira de bytes en la eeprom                    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

unsigned char tabla[]={'H','O','L','A','!'};
unsigned char size = sizeof(tabla)/sizeof(unsigned char);

/**********************************************/
/* Escribir un byte en la direccion indicada  */
/**********************************************/
void eeprom_write(unsigned char dir, unsigned char dato)
{
  EEADR=dir;     //-- Establecer direccion
  EEDATA=dato;   //-- Establecer el dato
  EEPGD=0;       //-- Indicar acceso a la memoria eeprom

  WREN=1;
  EECON2=0x55;
  EECON2=0xAA;
  WR=1;       //-- Comenzar la escritura!

  //-- Esperar a que la escritura termine
  while (EEIF==0);

  //-- Quitar flag de interrupcion
  EEIF=0;
}


void main(void)
{
  unsigned int i;

  TRISB1=0;

  //-- Escribir la tabla en la eeprom
  for (i=0; i<size; i++) {
    eeprom_write(i, tabla[i]);
  }

  //-- Rellenar lo que queda de eeprom con el caracter '.'
  for (i=size; i<255; i++) {
    eeprom_write(i,'.');
  }

  //-- Encender led para indicar que escritura completa
  RB1=1;

  while(1);
}
