;****************************************************************************
;*  plantilla.asm                                                           *
;---------------------------------------------------------------------------*
;  Plantilla ejemplo para programar los PICs en ensamblador                 *
;---------------------------------------------------------------------------*
;  LICENCIA GPL                                                             *
;****************************************************************************

 
; --- Especificar el PIC a emplear
  INCLUDE "p16f876a.inc"

;-- Palabra de configuracion
    __CONFIG _RC_OSC & _WDT_ON & _PWRTE_OFF & _BODEN_ON & _LVP_ON & _CPD_OFF & _WRT_OFF & _DEBUG_OFF & _CP_OFF

; -- Vector de reset
  ORG 0
  goto start

;-- Vector de interrupcion
  ORG 0x04
  ;-- Instrucciones
  retfie

;--- Programa principal
start

   ;- Instruccciones
fin goto fin

END

  


