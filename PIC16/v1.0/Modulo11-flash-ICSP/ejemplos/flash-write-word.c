/*************************************************************************** */
/* flash-write-word.c                                                        */
/*---------------------------------------------------------------------------*/
/* MEMORIA FLASH                                                             */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de escritura de una palabra en la flash                           */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED RB1

//------------------------------------------------------------------
//-- Escribir una palabra en la memoria flash
//--
//-- ENTRADAS:
//--  dir -> Direccion donde escribir la palabra
//--  dato-> Palabra a escribir (14 bits)
//------------------------------------------------------------------
void flash_write(unsigned int dir, unsigned int dato)
{
  //-- Establecer la direccion donde escribir
  EEADRH = dir>>8;
  EEADR = dir&0xFF;

  //-- Establecer la palabra a escribir
  EEDATH = dato>>8;
  EEDATA = dato&0xFF;

  //-- Indicar acceso a memoria flash
  EEPGD=1;

  //-- Habilitar la escritura
  WREN=1;

  //-- Deshabilitar interrupciones
  GIE=0;

  //-- Comenzar la escritura!!
  EECON2=0x55;
  EECON2=0xAA;
  WR=1;

  //-- Es necesario incluir 2 instrucciones nop. El PIC deja de ejecutar
  //-- codigo y espera hasta que la grabacion este completada
  _asm      
      nop
      nop
  _endasm;

  //-- Deshabilitar la escritura
  WREN=0;
}


void main(void)
{
  //-- Configurar LED de la skypic
  TRISB1=0;

  //-- Escribir la palabra 0x2A55 en la direccion 0x800
  flash_write(0x0800,0x2A55);

  //---------------------------------------------------------------------------
  //-- MUY IMPORTANTE!!!!!!
  //-- En los PIC16F87XA  (terminados en A), las escrituras en la flash
  //-- solo se pueden realizar en bloques de 4 palabras, en direcciones
  //-- consecutivas. Ademas, la direccion inicial tiene que ser multiplo de 4,
  //-- (los bits 0 y 1 tienen que estar a 0)
  //-- Si no se hace asi, la escritura no se realizara
  //---------------------------------------------------------------------------

  //-- Meter datos de relleno para que la gracion se lleve a cabo
  flash_write(0x0801,0x0001);
  flash_write(0x0802,0x0002);
  flash_write(0x0803,0x0003);

  //-- Encender led para indicar escritura completada
  LED=1;

  while(1);
}


