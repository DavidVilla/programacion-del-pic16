#include <pic16f876a.h>

/*****************************************/
/* Configurar el puerto serie            */
/*****************************************/
void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/***********************************************************************/
/* Leer un byte del SCI. Esta funcion se queda esperando hasta que     */
/* algun caracter llegue                                               */
/***********************************************************************/
unsigned char sci_read()
{
  unsigned char car;

  //-- Esperar hasta que se reciba un dato u ocurra un error de overflow
  while (RCIF==0 && OERR==0);

  //-- Leer dato (para que Flag se ponga a 0)
  car = RCREG;
  
  //-- Si ha ocurrido un error
  if (OERR==1) {
    //-- Reiniciar el receptor
    CREN=0;
    CREN=1;

    //-- Tomar la accion oportuna.
    //-- Esto depende de la aplicacion que se este progamando
  }
  
  //-- Devolver el caracter recibido
  return car;
}

/********************************************/
/* Enviar un byte por el SCI                */
/********************************************/
void sci_write(unsigned char dato)
{
  //-- Esperar a que Flag de lista para transmitir se active
  while (TXIF==0);
    
  //-- Hacer la transmision
  TXREG=dato;
}

/*******************************************/
/* Enviar una cadena por el SCI            */
/*******************************************/
void sci_cad(unsigned char *cad)
{
  unsigned char i=0;

  while (cad[i]!=0) {
    sci_write(cad[i]);
    i++;
  }
}

