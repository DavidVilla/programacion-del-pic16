/*************************************************************************** */
/* luces2.c                                                                  */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/*  Ejemplo de utilizacion de tablas en C para la generacion de secuencias   */
/* en los leds conectados al puerto B                                        */
/* Se utiliza el operador Sizeof() para determinar en tiempo de compilacion  */
/* el tamano de la tabla y usarlo en el bucle que la recorre. De esta manera */
/* se pueden anadir mas elementos a la secuencia de los leds solo            */
/* introduciendo valores en la tabla, sin tener que modificar nada mas del   */
/* codigo                                                                    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Tabla con los valores a sacar por los leds
unsigned char tabla[]={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};

//-- Otras secuencias de ejemplo:
//-- Ejemplo 1:
//-- {0xAA,0x55}

//-- Ejemplo 2: 
//-- {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80}

//-- Calculo del numero de elementos de la tabla
//-- Se pueden anadir elementos sin tener que especificar
//-- el tamano a mano
unsigned char size = sizeof(tabla)/sizeof(unsigned char);

/**********************************************/
/* Funcion de pausa                           */
/* ENTRADA: unidades de tiempo para la pausa  */
/**********************************************/
void pausa(unsigned char tiempo)
{
  unsigned char i;
  unsigned int j;
  int temp=0;

  //-- Se realizan dos bucles anidados
  for (i=0; i<tiempo; i++) {
    for (j=0; j<0x8000; j++) {
      //-- Operacion inutil, para consumir tiempo
      temp=temp+1;
    }
  }
}

void main(void)
{
  unsigned char i;

  //-- Configurar puerto B para salida
  TRISB=0x00;

  while(1) {

    //-- Recorrer todos los elementos de la tabla
    for (i=0; i<size; i++) {

      //-- Sacar el valor i por los leds
      PORTB=tabla[i];

      //-- Pausa
      pausa(2);
    }
  }

}

