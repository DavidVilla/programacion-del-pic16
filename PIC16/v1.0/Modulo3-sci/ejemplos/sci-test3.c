/*************************************************************************** */
/* sci-test3.c                                                               */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de uso de la libreria libsci. Las funciones de acceso al puerto   */
/* serie estan definidas en el archivo libsci.c que se compila por separado  */
/* y se crea la libreria libsci.lib                                          */
/* Este programa de ejemplo simplemente hace eco de todo lo que recibe       */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/
#include <pic16f876a.h>
#include "libsci.h"

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned char c;
  
  //-- Configurar el puerto B para salida
  TRISB=0;

  //-- Configurar puerto serie
  sci_conf();
  
  while(1) {

    //-- Leer dato (se queda esperando hasta que llega)
    c=sci_read();

    //-- Sacar dato recibido por los leds
    PORTB=c;

    //-- Hacer eco del carácter recibido. Enviarlo de vuelta al PC
    sci_write(c);
    
  }

}

