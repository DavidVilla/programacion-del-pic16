/*************************************************************************** */
/* timer0-pausa1ms.c                                                         */
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de como implementar una pausa de 1ms. El programa hace parpadear  */
/* el LED                                                                    */ 
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valor inicial del TMR0 para realizar una pausa de 1ms
//-- Divisor del prescar a 32
#define T0INI 100

#define LED RB1

/******************************/
/* Hacer una pausa de 1 ms    */
/******************************/
void pausa_1ms()
{
  //-- Dar valor inicial del timer
  TMR0=T0INI;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);

}

void main(void)
{
  TRISB1=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 32
  PS2=1; PS1=0; PS0=0;

  while(1) {
    pausa_1ms();
    LED=1;
    pausa_1ms();
    LED=0;
  }
}

