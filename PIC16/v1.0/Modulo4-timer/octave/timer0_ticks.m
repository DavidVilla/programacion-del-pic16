#------------------------------------------------------------------------------
#-- Calcular el numero de ticks que transcurrir para que el temporizador 0
#-- cronometre una duracion
#-------------------------------------------------------------------------------
# ENTRADAS:
#-- FOSC: Frecuencia del cristal en Mhz
#-- Duracion: Duracion a conseguir (en micro-seg)
#-------------------------------------------------------------------------------
# DEVUELVE:
#-- Una lista con todos los ticks para cada uno de los divisores posibles
#------------------------------------------------------------------------------
# EJEMPLO: calcular los ticks que deben transcurrir para realizar una pausa
#--  de 1ms con la tarjeta Skypic (que funciona a 20Mhz)
#
# octave> timer0_ticks(20,1000)
# ans =  NaN   NaN   NaN   NaN   156    78    39    20
#
# Esto significa que hay 4 soluciones posibles, correspondientes a valores
# de los divisores de 32, 64, 128 y 256
#------------------------------------------------------------------------------
function N=timer0_ticks(FOSC,Duracion)

#--- Divisores posibles para el Timer 0 
DIV = [2 4 8 16 32 64 128 256];

#-- Calcular el numero de ticks
N = round(FOSC*Duracion./(4*DIV));

#-- Solo son validos valores de N de 8 bits. 
#-- Si los valores son superiores a 255 se ponen a NaN
for i=1:length(N)
    if N(i)>255
      N(i)=NaN;
    endif
endfor;

return
