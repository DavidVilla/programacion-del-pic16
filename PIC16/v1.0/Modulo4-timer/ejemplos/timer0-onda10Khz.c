/*************************************************************************** */
/* timer0-onda10Khz.c                                                        */
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de generacion de una senal cuadrada de 10Khz. Se hace parpadear   */
/* el led a esa frecuencia. Como es superior a 50Hz, no sera visible. Pero   */
/* si se conecta un altavoz se podra escuchar el tono                        */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/


#include <pic16f876a.h>

//-- Valor inicial del TMR0 para realizar una pausa 
//-- corresponiente al simeperiodo de una onda de 10Khz
//-- Divisor del prescar a 2
#define T0INI 131

#define LED RB1

/************************************************************************/
/* Hacer una pausa generica, determinada por el valor inicial T0ini     */
/************************************************************************/
void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);

}

void main(void)
{
  TRISB1=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 2
  PS2=0; PS1=0; PS0=0;

  while(1) {
    timer0_delay(T0INI);
    LED=1;
    timer0_delay(T0INI);
    LED=0;
  }
}

