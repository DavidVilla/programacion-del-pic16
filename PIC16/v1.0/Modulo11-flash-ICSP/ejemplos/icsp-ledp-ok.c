/*************************************************************************** */
/* icsp-ledp-ok.c                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de grabacion de un programa por el icsp. El codigo correspondiente*/
/* al programa ledp (que hace parpadear el led de la skypic) esta almacenado */
/* en una tabla, que se envia por el icsp para ser grabada en la skypic      */
/* destino. La Skypic destino tiene que estar en modo grabacion.             */
/* Ademas de la grabacion, se realiza la verificacion, para comprobar que    */
/* se ha grabado con exito.                                                  */
/*                                                                           */
/* Si al arrancar la skypic el pulsador esta apretado, se entra en el modo   */
/* de borrado, donde se hace un borrado completo del PIC (incluida palabra   */
/* de configuracion)                                                         */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Valores iniciales del TMR0 para realizar diferentes pausas
//-- Divisor del prescar a 256
#define T_10ms 61
#define T_1ms  236
#define T_2ms  217
#define T_3ms  197
#define T_4ms  178
#define T_100us 250

#define TIEMPO_100ms 10
#define TIEMPO_200ms 20
#define TIEMPO_20ms   2
#define TIEMPO_500ms 50
#define TIEMPO_1seg  100

#define LED      RB1
#define PULSADOR RB0

#define APRETADO 0

//-- Pines por donde se sacan las senales de gracion
//-- En la Skypic destino las conexiones son:
//- CLOCK --> RB6
//- DATA ---> RB7
//- RESET --> MCLR

//-- En la Skypic grabadora las senales se sacan por estos pines:
#define CLOCK 1<<3    // Bit 3
#define DATA  1<<7    // Bit 7
#define RESET 1<<4    // Bit 4

//------------------------------------------------------------------------
//       CODIGOS DE LOS COMANDOS DEL PROTOCOLO ICSP DEL PIC 
//------------------------------------------------------------------------
#define READ_DATA   0x04  // Comando READ_DATA from Program Memory
#define LOAD_DATA   0x02  // Comando: LOAD DATA For Program Memory
#define BEG_ER_PRO  0x08  // Comando: BEGIN ERASE PROGRAMMING CICLE	
#define INC_ADDR    0x06  // comando: INCREMENT ADDRESS
#define LOAD_CONFIG 0x00  // Comando: LOAD CONFIGURATION
#define CHIP_ERASE  0x1F  // Comando: CHIP-ERASE

/*-- Codigo del programa a grabar por el ICSP --*/
__code unsigned int ledp1[]=
{0x0000, 0x118A, 0x120A, 0x2821, 0x0000, 0x30FD, 0x1683, 0x1303, 
 0x0086, 0x3002, 0x1283, 0x0686, 0x30FF, 0x00FF, 0x30FF, 0x2012,
 0x2809, 0x0008, 0x1283, 0x1303, 0x00A3, 0x087F, 0x00A2, 0x0822,
 0x0423, 0x1903, 0x2820, 0x30FF, 0x07A2, 0x1C03, 0x03A3, 0x2817,
 0x0008, 0x118A, 0x120A, 0x2805, 0x00,   0x00,   0x00,   0x00,
};

//-- tamano del programa a grabar
unsigned int ledp1_size = sizeof(ledp1)/sizeof(int);

//-- Lectura de palabras
unsigned char palabral;    //-- Parte baja
unsigned char palabrah;    //-- Parte alta

//-- Leer de la memoria flash
unsigned int flash_read(unsigned int dir)
{
  EEADRH = dir>>8;
  EEADR = dir&0xFF;
  EEPGD=1;
  RD=1;
  RD=1;
  RD=1;

  return EEDATH<<8 | EEDATA;
}

/******************************/
/* Hacer una pausa de 10ms    */
/******************************/
void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);

}

void delay(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}

/*****************************************/
/* Hacer un reset de la placa destino    */
/*****************************************/
void picp_reset() {
 
  //-- Hacer reset
  PORTB=RESET;
  
  //-- Esperar 20 ms
  delay(TIEMPO_20ms);
	
	//-- Desactivar reset
	PORTB=0;

  //-- Esperar 20 ms
  delay(TIEMPO_20ms);
}

/*------------------------------------------------------------------*/
/*  FUNCIONES DE IMPLEMENTACION DEL PROTOCOLO ICSP                  */
/*------------------------------------------------------------------*/

/*********************************************/
/* Enviar bits por DATA                      */
/* Se comienza por los menos significativos  */
/* ENTRADAS:                                 */
/*     -dato: Byte a enviar                  */
/*     -nbits: numero de bits a enviar       */
/*********************************************/
void write_bits(unsigned char dato, unsigned char nbits)
{
  unsigned char i;
  
  
  for (i=0; i<nbits; i++) {
    
    //-- Enviar bit menos significativo de dato
    if ((dato & 0x01)==0)
      PORTB = PORTB & ~(DATA);  //-- Enviar un cero
    else 
      PORTB = PORTB | DATA;     //-- Enviar un uno
    
    //-- Activar el reloj
    PORTB = PORTB | CLOCK;  //-- Flanco de subida
    PORTB = PORTB & ~CLOCK; //-- Flanco de bajada
    
    //-- Acceder al siguiente bit de dato
    dato = dato >> 1;
  }  
}

/**********************************************************************/
/* Leer a traves del ICSP                                             */
/* ENTRADA: Numero de bits a leer                                     */
/* DEVUELVE: los bits leidos. Se comienza a insertar los bits por el  */
/*    bit mas significativo                                           */
/**********************************************************************/
unsigned char read_bits(unsigned char nbits)
{
  unsigned char dato=0x00;
  unsigned char i;
  
  for (i=0; i<nbits; i++) {
    dato=dato>>1;
  
    //-- Flanco de reloj: subida y baja.
    //-- A continuacion el dato esta listo
    PORTB = PORTB | CLOCK;
    PORTB = PORTB & ~CLOCK;
  
    if ((PORTB & DATA)==0X00)
      dato=dato & ~0x80; //-- Recibido un 0
    else
      dato=dato | 0x80;  //-- Recibido un 1
  
  }
  
  return dato;
}

/**********************************/
/* Enviar un comando por el ICSP  */
/* ENTRADAS:                      */
/*   -cmd : Comando a enviar      */
/**********************************/
void send_cmd(unsigned char cmd)
{
  //-- Poner el reloj a cero
  PORTB = PORTB & ~CLOCK;
  write_bits(cmd,0x6);
  
  //-- Poner DATA a '1' para indicar fin del comando
  PORTB = PORTB | DATA;
  
  //-- Aqui debe haber al menos una espera de 1us
  //-- A una frecuencia de 20Mhz, el pic tarda en ejecutar cada
  //-- instruccion 200ns. Al menos se tienen que ejecutar 5
  //-- Ponemos 6 instrucciones nop
  _asm 
    nop
    nop
    nop
    nop
    nop
    nop
  _endasm;
}

/***************************************/
/* Enviar un dato por el ICSP          */
/* Son palabras de 14 bits             */
/***************************************/
void send_data2(unsigned int dato)
{
  unsigned char datol;

  datol=dato&0xFF;

  //-- Enviar bit de start (0)
  write_bits(0x00,1);

  //-- Enviar byte bajo
  write_bits(datol,8);

  //-- Enviar byte alto
  write_bits(dato>>8,7);

  //-- Pausa para enviar el siguiente comando
  timer0_delay(T_100us);

}

/*************************************************************/
/* Recibir un dato de 14 bits por el ICSP                    */
/* Se actualizan las variables globales palabral y palabrah  */
/*************************************************************/
unsigned int recv_data()
{
  //unsigned char i;
  
  //-- Configurar pin DATA para entrada
  TRISB = TRISB | DATA;
 
  //-- Leer bit de start y descartarlo 
  read_bits(1);
  
  //-- Leer 8 bits menos significativos
  palabral=read_bits(8);
  
  //-- Leer los 6 bits mas significativos + el bit de stop
  palabrah=read_bits(7);
  
  //-- Alinear la parte alta para que los bits menos significativos
  //-- esten a la derecha. Los dos mas significativos se ponen a 0
  palabrah=(palabrah>>1)&0x3F; 

  //-- Configurar pin DATA para salida
  TRISB = TRISB & ~DATA;

  //-- Pausa antes de realizar la siguiente lectura
  timer0_delay(T_100us);

  return palabrah<<8 | palabral;
}

/*-------------------------------------------------------------------*/

/****************************************************/
/* Error en la grabacion. Se hace parpadear el led  */
/****************************************************/
void error()
{
  for (;;) {
    LED=1;
    delay(TIEMPO_100ms);
    LED=0;
    delay(TIEMPO_1seg);
  }
}    


/***********************/
/* Programa principal  */
/***********************/
void main(void)
{
  unsigned int i;
  unsigned int j;
  unsigned int dato;
    
  //-- Configurar el puerto B
  //-- CLOCK y RESET son de salida
  //-- DATA es entrada/salida pero inicialemnte se pone como salida
  TRISB = ~(CLOCK | DATA | RESET| 0x02);
    
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 256
  PS2=1; PS1=1; PS0=1;

  //-- Esperar un tiempo antes de comenzar
  delay(TIEMPO_20ms);

  //-- Hacer reset del pic destino
  picp_reset();

  //-- Comprobar el modo de funcionamiento
  if (PULSADOR==APRETADO) {
    //-- Modo Borrado: Borrar la memoria del pic 
    send_cmd(CHIP_ERASE);
    timer0_delay(T_10ms);

    //-- Hacer que el led parpadee, para indicar modo borrado
    LED=1;
    delay(TIEMPO_500ms);
    LED=0;

    //-- Terminar
    while(1);
  }

  //-- Modo normal: grabar el programa ledp1, verificarlo y grabar palabra
  //-- de configuracion
    
  //-------------------------------------------------
  //-- FASE 1: Grabar ledp1
  //-------------------------------------------------
  for (i=0; i<ledp1_size; i++) {
    //-- Enviar el comando de Load data
    send_cmd(LOAD_DATA);

    //-- Enviar la palabra
    send_data2(ledp1[i]);

    //-- El comando ICSP de grabacion solo hay que ejecutarlo cada
    //-- 8 palabras cargadas
    if (i%8==7) {
      send_cmd(BEG_ER_PRO);
      timer0_delay(T_10ms);
    } 

    //-- Incrementar puntero
    send_cmd(INC_ADDR); 
  }

  //-------------------------------------------------
  //-- FASE 2: VERIFICACION 
  //-------------------------------------------------
  //-- Comenzar verificacion
  picp_reset();  

  for (i=0; i<ledp1_size; i++) {
    //-- Enviar comando READ_DATA
    send_cmd(READ_DATA); 
  
    //-- Leer la palabra
    dato=recv_data();

    if (dato!=ledp1[i]) error();

    //-- Incrementar puntero
    send_cmd(INC_ADDR);
  }

  //-------------------------------------------------
  //-- FASE 3: Grabar Palabra de configuracion 
  //-------------------------------------------------
  //-- Se graba la palabra 0x3F3A

  //-- Reset del pic destino
  picp_reset();

  //-- Acceso a la memoria de configuracion
  send_cmd(LOAD_CONFIG); 
  send_data2(0x00);

  //-- Avanzar 7 posiciones hasta la direccion 2007
  for (j=0; j<7; j++) 
    send_cmd(INC_ADDR);

  //-- Enviar el comando de Load data
  send_cmd(LOAD_DATA);

  //-- Enviar la palabra
  send_data2(0x3F3A);

  //-- Grabarla!
  send_cmd(BEG_ER_PRO);

  //---------------------------------------------
  //-- Grabacion finalizada con exito!!!!!
  //---------------------------------------------
  //-- Encender el led para indicar que todo OK!
  LED=1;

  while(1);
    
}


