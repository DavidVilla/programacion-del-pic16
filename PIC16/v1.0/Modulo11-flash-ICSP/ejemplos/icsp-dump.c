/*************************************************************************** */
/* icsp-dump.c                                                               */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Volcar un bloque de memoria flash de otro PIC, por ICSP                   */
/* Los datos se envian por el puerto serie                                   */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>
#include "sci.h"

//-- Valores iniciales del TMR0 para realizar diferentes pausas
//-- Divisor del prescar a 256
#define T_10ms 61
#define T_100us 250

#define TIEMPO_20ms   2

//-- Led de la Skypic
#define LED      RB1

//-- Numero de palabras a volcar
//-- El usuario puede cambiar este valor (entre 0 y 0x2000)
#define TAM      0xF0

//-- Pines por donde se sacan las senales de gracion
//-- En la Skypic destino las conexiones son:
//- CLOCK --> RB6
//- DATA ---> RB7
//- RESET --> MCLR

//-- En la Skypic grabadora las senales se sacan por estos pines:
#define CLOCK 1<<3    // Bit 3
#define DATA  1<<7    // Bit 7
#define RESET 1<<4    // Bit 4

//------------------------------------------------------------------------
//       CODIGOS DE LOS COMANDOS DEL PROTOCOLO ICSP DEL PIC 
//------------------------------------------------------------------------
#define READ_DATA   0x04  // Comando READ_DATA from Program Memory
#define LOAD_DATA   0x02  // Comando: LOAD DATA For Program Memory
#define BEG_ER_PRO  0x08  // Comando: BEGIN ERASE PROGRAMMING CICLE	
#define INC_ADDR    0x06  // comando: INCREMENT ADDRESS
#define LOAD_CONFIG 0x00  // Comando: LOAD CONFIGURATION
#define CHIP_ERASE  0x1F  // Comando: CHIP-ERASE


//-- Lectura de palabras
unsigned char palabral;    //-- Parte baja
unsigned char palabrah;    //-- Parte alta

/******************************/
/* Hacer una pausa de 10ms    */
/******************************/
void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra 1ms
  while(T0IF==0);

}

void delay(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}

/*****************************************/
/* Hacer un reset de la placa destino    */
/*****************************************/
void picp_reset() {
 
  //-- Hacer reset
  PORTB=RESET;
  
  //-- Esperar 20 ms
  delay(TIEMPO_20ms);

  //-- Desactivar reset
  PORTB=0;

  //-- Esperar 20 ms
  delay(TIEMPO_20ms);
}

/*------------------------------------------------------------------*/
/*  FUNCIONES DE IMPLEMENTACION DEL PROTOCOLO ICSP                  */
/*------------------------------------------------------------------*/

/*********************************************/
/* Enviar bits por DATA                      */
/* Se comienza por los menos significativos  */
/* ENTRADAS:                                 */
/*     -dato: Byte a enviar                  */
/*     -nbits: numero de bits a enviar       */
/*********************************************/
void write_bits(unsigned char dato, unsigned char nbits)
{
  unsigned char i;
  
  
  for (i=0; i<nbits; i++) {
    
    //-- Enviar bit menos significativo de dato
    if ((dato & 0x01)==0)
      PORTB = PORTB & ~(DATA);  //-- Enviar un cero
    else 
      PORTB = PORTB | DATA;     //-- Enviar un uno
    
    //-- Activar el reloj
    PORTB = PORTB | CLOCK;  //-- Flanco de subida
    PORTB = PORTB & ~CLOCK; //-- Flanco de bajada
    
    //-- Acceder al siguiente bit de dato
    dato = dato >> 1;
  }  
}

/**********************************************************************/
/* Leer a traves del ICSP                                             */
/* ENTRADA: Numero de bits a leer                                     */
/* DEVUELVE: los bits leidos. Se comienza a insertar los bits por el  */
/*    bit mas significativo                                           */
/**********************************************************************/
unsigned char read_bits(unsigned char nbits)
{
  unsigned char dato=0x00;
  unsigned char i;
  
  for (i=0; i<nbits; i++) {
    dato=dato>>1;
  
    //-- Flanco de reloj: subida y baja.
    //-- A continuacion el dato esta listo
    PORTB = PORTB | CLOCK;
    PORTB = PORTB & ~CLOCK;
  
    if ((PORTB & DATA)==0X00)
      dato=dato & ~0x80; //-- Recibido un 0
    else
      dato=dato | 0x80;  //-- Recibido un 1
  
  }
  
  return dato;
}

/**********************************/
/* Enviar un comando por el ICSP  */
/* ENTRADAS:                      */
/*   -cmd : Comando a enviar      */
/**********************************/
void send_cmd(unsigned char cmd)
{
  //-- Poner el reloj a cero
  PORTB = PORTB & ~CLOCK;
  write_bits(cmd,0x6);
  
  //-- Poner DATA a '1' para indicar fin del comando
  PORTB = PORTB | DATA;
  
  //-- Aqui debe haber al menos una espera de 1us
  //-- A una frecuencia de 20Mhz, el pic tarda en ejecutar cada
  //-- instruccion 200ns. Al menos se tienen que ejecutar 5
  //-- Ponemos 6 instrucciones nop
  _asm 
    nop
    nop
    nop
    nop
    nop
    nop
  _endasm;
}

/***************************************/
/* Enviar un dato por el ICSP          */
/* Son palabras de 14 bits             */
/***************************************/
void send_data2(unsigned int dato)
{
  unsigned char datol;

  datol=dato&0xFF;

  //-- Enviar bit de start (0)
  write_bits(0x00,1);

  //-- Enviar byte bajo
  write_bits(datol,8);

  //-- Enviar byte alto
  write_bits(dato>>8,7);

  //-- Pausa para enviar el siguiente comando
  timer0_delay(T_100us);

}

/*************************************************************/
/* Recibir un dato de 14 bits por el ICSP                    */
/* Se actualizan las variables globales palabral y palabrah  */
/*************************************************************/
unsigned int recv_data()
{
  //unsigned char i;
  
  //-- Configurar pin DATA para entrada
  TRISB = TRISB | DATA;
 
  //-- Leer bit de start y descartarlo 
  read_bits(1);
  
  //-- Leer 8 bits menos significativos
  palabral=read_bits(8);
  
  //-- Leer los 6 bits mas significativos + el bit de stop
  palabrah=read_bits(7);
  
  //-- Alinear la parte alta para que los bits menos significativos
  //-- esten a la derecha. Los dos mas significativos se ponen a 0
  palabrah=(palabrah>>1)&0x3F; 

  //-- Configurar pin DATA para salida
  TRISB = TRISB & ~DATA;

  //-- Pausa antes de realizar la siguiente lectura
  timer0_delay(T_100us);

  return palabrah<<8 | palabral;
}

/*-------------------------------------------------------------------*/


/***********************/
/* Programa principal  */
/***********************/
void main(void)
{
  unsigned int i;
  unsigned int dato;
    
  //-- Configurar el puerto B
  //-- CLOCK y RESET son de salida
  //-- DATA es entrada/salida pero inicialemnte se pone como salida
  TRISB = ~(CLOCK | DATA | RESET| 0x02);
    
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 256
  PS2=1; PS1=1; PS0=1;

  //-- Inicializar puerto serie
  sci_conf();
    
  //-- Esperar un tiempo antes de comenzar
  delay(TIEMPO_20ms);

  //-- Hacer reset del pic destino
  picp_reset();

  //-- Realizar el volcado!!
  for (i=0; i<TAM; i++) {
    //-- Sacar 16 palabras por cada linea
    if (i%16==0) sci_write('\n');
      
    //-- Enviar comando READ_DATA
    send_cmd(READ_DATA); 
  
    //-- Leer la palabra
    dato=recv_data();

    //-- Enviarla por el puerto serie
    sci_hexint(dato);
    sci_write(' ');

    //-- Incrementar puntero
    send_cmd(INC_ADDR);
  }

  //-- Encender el led para indicar que volcado finalizado
  LED=1;

  while(1);
}


