/*******************************************************/
/* Brujula                                 Agosto-2006 */
/*-----------------------------------------------------*/
/* Programacion del BUS I2C para el PIC 16F876A        */
/*******************************************************/

//-- Especificar el pic a emplear
#include "pic16f876a.h"

#include "sci.h"

// DEFINICIONES RELACIONADAS CON LOS MOTORES

//-- Identificadores de los servicios
#define SID   'I'   // Servicio de identificacion (0x49)
#define SPING 'P'   // Servicio de PING (0x50)

//-- Identificadores para la respuesta
#define RPING 'O'   // Respuesta al Servicio de PING (0x4F)
#define RSI   'I'   // RSI. Codigo de respuesta del servicio de identificacion

//----- Datos devueltos por el servicio de identificacion
#define IS	0x42    // IS. Identificacion del servidor
#define IM	0x30    // IM. Identificacion del microcontrolador
#define IPV	0x10    // IPV. Placa custom. Version 0.


//-- Definiciones relacionadas con el sensor
#define LED       0x02     // Pin del led de la Skypic
#define CMPS03    0xC0     // Direccion del chip de la brujula


#define REVISION  '1'   // comando para leer la revision del firware del cmps03 (brujula)
#define DIR_B     '2'   // comando para leer la direccion en un byte del cmps03 (brujula)
#define DIR_W      '3'  // comando para leer la direccion en dos bytes del cmps03 (brujula)

//-- declaracion de funciones
unsigned char cmps03_read_version();
unsigned char cmps03_read_dir_short();
unsigned int  cmps03_read_dir_long();


//-- variables globales
int relacion=0;
int car;  // Caracter recibido por el SCI

/****************************
	Servicios del servidor
****************************/

void serv_cmps03_rev() {
  unsigned char valor;
  
  valor=cmps03_read_version();
  sci_write( valor );
}


void serv_cmps03_read_dir() {
  char valor;
  
  valor=cmps03_read_dir_short();
  sci_write( valor );
}


void serv_cmps03_read_ldir() {
  unsigned int valor;
  
  valor=cmps03_read_dir_long();
  
  sci_write( (valor>>8) & 0xFF);
  sci_write( valor&0xFF );
}


/****************************
	FUNCIONES AUXILIARES
****************************/


/************************************************/
/* Hacer una pausa en unidades de 10ms          */
/* OJO: No es buena practica porque depende de  */
/* factores externos como optimizaci�n del      */
/* compilador o la propia frecuencia del micro  */
/*                                              */
/* Lo bueno es que una vez calibrada tenemos una*/
/* medida que no consume un TIMER               */
/************************************************/
void delay(int pausa)   
{
 unsigned int tmp;

  //-- Esperar hasta que trancurran pausa ticks de reloj
  while(pausa>0) {
    tmp=0xFFF;
  while (tmp>0) tmp--;
  pausa--;
  } 
}


/*
****************************************
*    FUNCIONES DE BAJO NIVEL DEL I2C   * 
****************************************
*/

// Envia Ack
// SDA = 0 
void i2c_SendAck() {
  ACKDT = 0;  // Establece un ACK
  ACKEN = 1;  // Lo envia
}


// Envia Nack para finalizar la recepecion
// SDA = 1
void i2c_SendNack() {
  ACKDT = 1;  // Establece un NACK
  ACKEN = 1;  // Lo envia
}


// verifica ACK
// Mira si en el 9 pulso de reloj (SCL en estado a 1) la se�al SDA est� a 0
unsigned char i2c_CheckACK() {
  if ( ACKSTAT == 0 ) {
    return 0;   // correcto (lo he recibido )
  } else { 
    return 1;	// incorrecto (no lo he recibido)
  }
}


// Envia la condicion de STOP
// Libera el BUS I2C, SDA y SCL a nivel alto
// SDA=1 cuando SCL=1. 
void i2c_SendStop() {
  PEN = 1;    // send stop bit
}


// Envia la condicion de START
// Inicializa el Bus I2C, SCL y SDA a nivel bajo
// Estando SCL=1 pone SDA=0, luego pone SCL=0
void i2c_SendStart() {
  SEN = 1;      // send start bit
}

// Espera a que el I2C reciba algun evento
void i2c_WaitMSSP() {
  while (SSPIF == 0);  // Espera evento
  SSPIF=0;             // Limpia FLAG
}

// Repeated start from master
// Queremos transmitir m�s datos pero sin dejar el BUS, es decir
// sin mandar previamente un STOP. O por ejemplo si queremos mandar un STOP y seguidamente un
// START para que ning�n otro dispositivo ocupe la l�nea usaremos esto.
void i2c_SendRStart() {
  RSEN=1;
}


// Leer Byte por el I2C
// Devuelve byte leido
unsigned char i2c_ReadByte() {
  RCEN=1;        // Activar el RCEN

  i2c_WaitMSSP();
  return SSPBUF;
}




// Envia un Dato por el Bus I2C
// Entrada el dato a enviar
void i2c_SendByte(unsigned char dato) {
  SSPBUF=dato;
}



// Fallo en el I2C
void i2c_fail() {
  i2c_SendStop();
  i2c_WaitMSSP();
  sci_write('f');   // @@@ Para depurar
}




/**********************************
 FUNCIONES DE ALTO NIVEL
***********************************/

/* Configurar I2C
	Configuramos el I2C como Master
	y a una velocidad de 1Mhz
*/

void i2c_configure() {
  // configuramos SCL y SDA como pines de entrada
  TRISC=TRISC | 0x18;  // 1 entrada / 0 salida  
  
  SSPSTAT=0x0;  
  SSPSTAT=SSPSTAT | 0x80;  // SMP=1 Slew rate disable for 100Kh  @@@
                         // CKE=0 (modo maestro I2C) y UA=0 (7 bits)

  SSPCON=0x08;	 		 // I2C master mode (uso la formula del reloj con SSPAD)  
  SSPCON=SSPCON | 0x20;	 // enable I2C  SSPEN=1
  
  SSPCON2=0x00;   // no se usa por ahora
  // velocidad = FOSC / ( 4 * (sspad + 1) )
  // Fosc=20Mhz
  
  SSPADD=49;  // 49->100Khz  @@@
  //SSPADD=24; // 24 -> 400khz   @@@
  
  // Limpia Flags de eventos
  SSPIF=0;  //Limpia flag de eventos SSP 
}



// manda un byte al dispositivo i2c
unsigned char i2c_write_byte(unsigned char address, unsigned char reg, unsigned char dato)
{

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  	
  
  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;  // Ha habido un error, salgo de la rutina
  }
  
  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg);
  i2c_WaitMSSP(); 
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� mando el dato 
  i2c_SendByte(dato); 
  i2c_WaitMSSP();
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    return 0;    // Ha habido un error, salgo de la rutina
  }

  // 6� termino el envio
  i2c_SendStop();
  i2c_WaitMSSP();

  return 1;  // Byte mandado correctamente
}




// Lee 1 byte1  del dispositivo i2c
// El dato leido lo devuelve por el parametro dato
unsigned char i2c_read_byte(unsigned char address, unsigned char reg, unsigned char *dato)
{

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  	

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    sci_write('a');
    return 0;   // Ha habido un error, salgo de la rutina
  }
  
  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg); 
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    sci_write('b');
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� Repeated start
  i2c_SendRStart();
  i2c_WaitMSSP();  
  
  // 4� mando direccion indicando lectura
  i2c_SendByte(address+1);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    sci_write('c');
    return 0;   // Ha habido un error, salgo de la rutina
  }
  
  // 5� leo el byte 
  *dato=i2c_ReadByte(); 
  
  // 6� Mando NACK
  i2c_SendNack();
  i2c_WaitMSSP();  
  
  // Mando el Stop
  i2c_SendStop();
  i2c_WaitMSSP();  

  return 1;   // Lectura correcta
}


// Lee 2 bytes consecutivos  del dispositivo i2c
// El dato leido lo devuelve por el parametro dato
unsigned char i2c_read_word(unsigned char address, unsigned char reg, unsigned int *dato)
{
  unsigned char tmp_H, tmp_L;

  // 1� Envia Bit de Start
  i2c_SendStart();
  i2c_WaitMSSP();  	

  // 2� Envio la direccion del modulo
  i2c_SendByte(address);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    sci_write('a');
    return 0;   // Ha habido un error, salgo de la rutina
  }
  
  // 3� mando el registro sobre el que voy a actuar
  i2c_SendByte(reg); 
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    sci_write('b');
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 4� Repeated start
  i2c_SendRStart();
  i2c_WaitMSSP();  
  
  // 4� mando direccion indicando lectura
  i2c_SendByte(address+1);
  i2c_WaitMSSP();  
  if (i2c_CheckACK()!=0) {
    i2c_fail();
    sci_write('c');
    return 0;   // Ha habido un error, salgo de la rutina
  }

  // 5� leo el 1 byte 
  tmp_H=i2c_ReadByte(); 
  
  // 6� Mando ACK
  i2c_SendAck();
  i2c_WaitMSSP();  
  
  // 7� leo el 2 byte 
  tmp_L=i2c_ReadByte(); 
  
  // 6� Mando NACK
  i2c_SendNack();
  i2c_WaitMSSP();  
  
  // Mando el Stop
  i2c_SendStop();
  i2c_WaitMSSP();  

  *dato=(tmp_H*256) + tmp_L;

  return 1;   // Lectura correcta
}


/****************************** 
      Funciones de alto nivel
    para el sensor cmps03
    (brujula)
******************************/

unsigned char cmps03_read_version() {
  unsigned char res; 
  i2c_read_byte(CMPS03,0x00, &res);
  
  return res;
}

unsigned char cmps03_read_dir_short() {
  unsigned char res; 
  i2c_read_byte(CMPS03,0x01,&res);
  return res;
}


unsigned int cmps03_read_dir_long() {
  unsigned int res;
  
  i2c_read_word(CMPS03,0x02,&res);
  
  return res;
}



//----------------------------
//- Comienzo del programa  
//----------------------------

void main(void)
{
  unsigned char tecla;
  int ret=0;
  
  //-- configura como salida el Puerto B excepto bit 0
  TRISB=0x01;
  PORTB=0x02;  // enciende el LED
  
  //-- Configurar el puerto serie
  sci_conf();
  
  //-- Configurar el I2C
  i2c_configure(); 
  
  //-- Hago una pausa antes de empezar
  delay(100);
  
  // Activar las interrupciones
  //BSF INTCON,GIE
  
  while (1) {
    // leer_tecla();
    tecla = sci_read();
  
    switch (tecla) {

    case '1':  // lectura de la version del software
          serv_cmps03_rev();
           break;

    case '2': // lectura byte direccion
          serv_cmps03_read_dir();
          break;

    case '3': // lectura word direccion
          serv_cmps03_read_ldir();
          break;
        
    default:  sci_write('*');
          break;
    }
  //-- Cambiar el led de estado
  PORTB^=0x02;
  }
} 
