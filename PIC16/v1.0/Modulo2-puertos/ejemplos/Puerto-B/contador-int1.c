/*************************************************************************** */
/* contador-int1.c                                                           */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de uso de la entrada RB0. Cada vez que se reciba un flanco por    */
/* de bajada por RB0 se cambiara el estado del led                           */
/* Para detectar el flanco se lee el flag INTF                               */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

void main(void)
{
  //-- Configuracion puerto B: Todos los bits de salida
  //-- excepto RB0 que es de entrada
  TRISB=0x01;

  //-- Configurar interrupcion por flanco de bajada
  INTEDG=0;

  //-- Bucle principal
  while(1) {

    //-- Poner flag de interrupcion a 0
    INTF=0;

    //-- Esperar a que llegue el flanco
    while(INTF==0);

    //-- Cambiar el LED de estado
    //-- Otra manera: RB1^=1
    RB1=RB1^1;

    //-- Opcional: anadir una pausa
    //-- El pulsador pude generar "rebotes" y aparecer flancos no deseados
  }

}

