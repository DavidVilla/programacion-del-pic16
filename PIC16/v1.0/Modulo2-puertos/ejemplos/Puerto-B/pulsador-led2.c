/*************************************************************************** */
/* pulsador-led2.c                                                           */
/*---------------------------------------------------------------------------*/
/* ENTRADA/SALIDA DIGITAL: PUERTO B                                          */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de entrada y salida. Se configura el pin RB0 como entrada y       */
/* RB1 como salida. Por RB1 (el led) se saca el valor negado de lo recibido  */
/* por RB0 (pulsador)                                                        */
/* Mejora de la legibilidad del programa usando directivas #define           */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

#define LED      RB1
#define PULSADOR RB0
#define ON       1
#define OFF      0
#define APRETADO 0
#define CONFIGURAR_LED	    TRISB0=1
#define CONFIGURAR_PULSADOR TRISB1=0

void main(void)
{
  CONFIGURAR_LED;
  CONFIGURAR_PULSADOR;

  while(1) {
    if (PULSADOR==APRETADO) {
      LED=ON;
    } 
    else {
      LED=OFF;
    }
  }

}

