/*************************************************************************** */
/* timer0-delay.c                                                            */
/*---------------------------------------------------------------------------*/
/* TEMPORIZADOR 0                                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de generacion de una pausa "larga". Se hace parpadear el led con  */
/* una frecuencia de 1Hz                                                     */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/
#include <pic16f876a.h>

//-- Valor inicial del TMR0 para realizar una pausa de 10ms
//-- Divisor del prescar a 256
#define T_10ms 61

#define LED RB1

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/*******************************************************/
/* Pausa                                               */
/* ENTRADA: duracion de la pausa en centesimas (10ms)  */
/*******************************************************/
void timer0_delay_long(unsigned int duracion)
{
  unsigned int i;

  for (i=0; i<duracion; i++)
    timer0_delay(T_10ms);
}

void main(void)
{
  TRISB1=0;
  LED=0;
  
  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler a 256
  PS2=1; PS1=1; PS0=1;

  //-- Hacer parpadear el led con periodo de 1segundo
  while(1) {
    //-- Cambiar led de estado
    LED^=1;
    //-- Pausa de 0.5 segundos
    timer0_delay_long(50);
    
  }
}

