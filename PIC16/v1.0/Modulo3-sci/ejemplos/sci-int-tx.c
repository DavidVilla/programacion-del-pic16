/*************************************************************************** */
/* sci-int-tx.c                                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para el PUERTO SERIE                                              */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de interrupcion de dato recibido. Se envia una cada mediante      */
/* interrupciones. El bucle principal no hace nada                           */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

unsigned char i=0;
unsigned char cad[]={'h','o','l','a',0};

void sci_conf()
{
  //-- Configurar valocidad puerto serie
  //--  Velocidad SPBRG (dec)
  //--   9600      129
  //--   19200     64 
  //--   57600     20
  BRGH=1;
  SPBRG=129;  //-- 9600

  //-- Configuracion puerto serie
  SYNC=0;  //-- Comunicacion asincrona
  SPEN=1;  //-- Habilitar puerto serie (pines)
  CREN=1;  //-- Habilitar receptor
  TXEN=1;  //-- Habilitar transmisor
}

/*****************************************/
/* Rutina de atencion a la interrupcion  */
/*****************************************/
void isr() interrupt 0 
{
  //-- Enviar el siguiente caracter de la cadena, si no es el ultimo
  if (cad[i]!=0) {

    //-- Enviar caracter i
    TXREG=cad[i];

    //-- Apuntar al siguiente caracter a enviar
    i++;
  }
}

/*********************************/
/* Programa principal            */
/*********************************/
void main(void)
{
  unsigned char cont=0;

  //-- Configurar puerto B para salida
  TRISB=0;

  //-- Configurar puerto serie
  sci_conf();

  //-- Activar las interrupciones
  TXIE=1;
  PEIE=1;
  GIE=1;
  
  while(1);
}

