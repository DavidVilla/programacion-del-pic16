;****************************************************************************
;*  ledp.asm                                                                *
;---------------------------------------------------------------------------*
; Ejemplo para la tarjeta SKYPIC                                            *
;---------------------------------------------------------------------------*
; Hacer parpadear el led (bit RB1)                                          *
; La pausa se hace mediante un contador (no se usan temporizadores)         *
;---------------------------------------------------------------------------*
;  LICENCIA GPL                                                             *
;****************************************************************************

;-- Establecer el PIC a emplear
  INCLUDE "p16f876a.inc"

;---------------------
; VARIABLES
;---------------------
	cblock	0x20
	CONTH			; Informaci�n a enviar
	CONTL			; N�mero de bits a enviar
	endc

  ORG 0
  CLRF    STATUS
  MOVLW   0
  MOVWF   PCLATH
  GOTO    start

start
;-- Configurar puerto B: RB0 entrada, RB1 para salida
  BSF STATUS,RP0  ; Cambiar al banco 1
  BCF TRISB,1     ; Poner RB1 como salida

;-- Cambiar al banco 0
  BCF STATUS,RP0


;-- Bucle principal

main
;-- Cambiar el bit RB1 de estado
  MOVLW 0x02
  XORWF PORTB,F

;-- Hacer una pausa
  CALL pausa
  GOTO main

;******************************************************************
; Hacer una pausa. Se utiliza una variable de 16 bits,
; que hace de contador. 
; Esta constituida por CONTH (parte alta) y CONTL (parte baja)
; El contador se inicializa a 0xFFFF y se va decrementando
; Se hace con dos bucles anidados, uno que decrementa CONTL y
; cuando llega a cero decrementa CONTH y repite el bucle
;******************************************************************
pausa
  MOVLW 0xFF    ; Inicializar parte alta contador
  MOVWF CONTH

bucle1
  MOVLW 0xFF    ; Inicializar parte baja contador
  MOVWF CONTL
bucle2
  DECFSZ CONTL,F  ; Decrementa CONTL �CONTL=0?
  goto bucle2     ;  NO--> Repite

  DECFSZ CONTH,F  ; Decrementa CONTH �CONTH=0?
  goto bucle1     ; No--> Ve a buclel

;-- Si se ha llegado aqu� es porque el contador ha llegado a 0000
;-- (CONTH=0 y CONTL=0)

  RETURN

END
