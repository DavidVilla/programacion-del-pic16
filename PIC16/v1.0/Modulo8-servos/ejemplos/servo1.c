/*************************************************************************** */
/* servo1.c                                                                  */
/*---------------------------------------------------------------------------*/
/* CONTROL DE SERVOS                                                         */
/*---------------------------------------------------------------------------*/
/* Ejemplo para la tarjeta SKYPIC                                            */
/*---------------------------------------------------------------------------*/
/* Ejemplo de posicionamiento de un servo del tipo Futaba 3003               */
/* La senal se genera mediante espera activa. Se usa el timer 0              */
/* El servo se lleva a la posicion -90, luego a la 0 y luego a la 90.        */
/* La secuencia se repite indefinidamente                                    */
/*---------------------------------------------------------------------------*/
/*  LICENCIA GPL                                                             */
/*****************************************************************************/

#include <pic16f876a.h>

//-- Prescaler a 64
//-- Ventana temporal para el servo en ticks (2.5ms)
#define T           195
#define T0INI_2_5ms 256-195

void timer0_delay(unsigned char t0ini)
{
  //-- Dar valor inicial del timer
  TMR0=t0ini;
 
  //-- Flag de interrupcion a cero
  T0IF=0;

  //-- Esperar a que transcurra el tiempo indicado
  while(T0IF==0);
}

/**************************************************************************/
/* Generar una senal PWM para posicionar el servo con el angulo indicado  */
/* El angulo debe estar en el rango (-90,90)                              */
/**************************************************************************/
void servo_pos(int pos) 
{
  unsigned int ciclos;
  unsigned int i;
  char Ton;

  //-- Calcular el ancho del pulso en funcion de los grados
  //-- La posicion central se corresponde con 1.3ms. Un extremo con 2.3 y 
  //-- el otro con 0.3
  Ton = (78*pos)/90 + 102;
  
  for (ciclos=0; ciclos<100; ciclos++) {

    //-- Realizar el PWM, centro de una ventana de 2.5ms
    PORTB=0xFF;
    timer0_delay(255-Ton);
    PORTB=0x00;
    timer0_delay(255-T+Ton);
  
    //-- completar con ventanas de 2.5 hasta alcanzar los 20ms
    for (i=0; i<7; i++)
      timer0_delay(T0INI_2_5ms);
  }
}


void main(void)
{
  //-- Puerto B de salida
  TRISB=0;

  //-- Configurar Timer 0
  //-- Modo temporizador
  T0CS=0; PSA=0;

  //-- Presscaler A 64
  PS2=1; PS1=0; PS0=1;

  while(1) {
    servo_pos(-90);  //-- un extremo
    servo_pos(0);    //-- centro
    servo_pos(90);   //-- Otro extremo
  } 

}



